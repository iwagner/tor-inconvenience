import os
import argparse
import json

import pandas as pd
from tranco import Tranco
from selenium.webdriver import Firefox, FirefoxProfile
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import ElementNotInteractableException, StaleElementReferenceException, \
    TimeoutException, NoSuchWindowException, JavascriptException, NoSuchElementException, WebDriverException
from selenium.webdriver.support.ui import WebDriverWait
import tbselenium.common as cm
from tbselenium.tbdriver import TorBrowserDriver
from tbselenium.utils import launch_tbb_tor_with_stem
from pathlib import Path
from datetime import datetime, time
import time
import requests
import random

import setup
import get_subsites

if os.environ.get('AM_I_IN_A_DOCKER_CONTAINER', default=False):
    # running in Docker
    # headless pyautogui magic, see:
    # https://abhishekvaid13.medium.com/pyautogui-headless-docker-mode-without-display-in-python-480480599fc4
    from pyvirtualdisplay.display import Display
    import Xlib.display

    disp = Display(visible=True, size=(1366, 1024), backend="xvfb", use_xauth=False)
    disp.start()
    import pyautogui
    pyautogui._pyautogui_x11._display = Xlib.display.Display(os.environ['DISPLAY'])
else:
    import pyautogui


def main():
    # Parse flags
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", help="number of top sites to use", metavar="100", default="2", type=int)
    parser.add_argument("--first", help="index of first top site to use; when used with the Hispar argument: "
                                        "index of first subsite to use for each domain in the Hispar list",
                        default=0, type=int)
    parser.add_argument("--sites-csv", help="CSV file listing one site per line")
    parser.add_argument("--subsites", help="number of sub_sites to retrieve", metavar="5", default="0", type=int)
    parser.add_argument("-e", help="number of random Tor exit nodes to use", metavar="10", default="2", type=int)
    parser.add_argument("--exit-nodes", help="IP address(es) of exit node(s) to use", nargs='*')
    parser.add_argument("--exit-csv", help="CSV file listing one exit node per line")
    parser.add_argument("--setup", "-s", help="force setup to run", action="store_true")
    parser.add_argument("--tranco-date", "-d", help="date to use when requesting the Tranco top sites list. uses latest if not supplied",
                         metavar="YYYY-MM-DD", default='2021-05-11')
    parser.add_argument("--hispar", help="use Hispar toplist and their subsites insted of Tranco", action="store_true")
    parser.add_argument("--hispar-idx", help="index of the Hispar topsite to use", type=int, default="-1")
    args = parser.parse_args()

    # Load settings if they exist, else run setup.py
    if setup.check_file_exists("settings.json") and args.setup is False:
        print("Settings file exists, skipping setup. Run with --setup to force setup to run.")
    else:
        setup.main()
    with open("settings.json", 'r') as f:
        paths = json.load(f)

    if args.sites_csv:
        top_sites = list(pd.read_csv(args.sites_csv, header=None)[0].values)
    elif args.hispar:
        if args.hispar_idx >= 0:
            top_sites = [pd.read_csv('2021-10-20-hispar-topsites.csv', header=None)[0].values[args.hispar_idx]]
        else:
            top_sites = list(pd.read_csv('2021-10-20-hispar-topsites.csv', header=None)[0].values)
    else:
        # Fetch tranco list of top sites
        t = Tranco(cache=True, cache_dir='.tranco')
        if args.tranco_date == None:
            tranco_list = t.list()
        else:
            tranco_list = t.list(date=args.tranco_date)
        top_sites = tranco_list.top(int(args.n) + int(args.first))[args.first:]

    # Get current date and time to create test identifier
    now = datetime.now()
    timestamp = now.strftime("%Y-%m-%d_%H%M%S")

    # Set directory to store data in for this test
    paths["base"] = Path(__file__).resolve().parent.as_posix()

    if os.environ.get('AM_I_IN_A_DOCKER_CONTAINER', default=False):
        # running in Docker
        paths["data"] = Path("/root/shared/results/").resolve()
    else:
        paths["data"] = Path(__file__).resolve().parent / "data" / timestamp
    paths["data"].mkdir(parents=True, exist_ok=True)

    # Save sites list as csv for later reference
    with open(f"{str(paths['data'])}/sites.csv", "w") as f:
        for site in top_sites:
            f.write(f"{str(top_sites.index(site)+1)},{site}\n")

    # Get list of subsite URLs
    if args.subsites > 0:
        if args.sites_csv:
            sub_sites = get_subsites.get_subsites_from_csv(args.sites_csv, args.subsites)
        elif args.hispar:
            sub_sites = get_subsites.get_subsites_from_hispar(args.subsites, args.first)
        else:
            sub_sites = get_subsites.get_subsites_from_idx(args.n, args.subsites, args.first)
        sub_sites.to_csv(f"{str(paths['data'])}/subsites.csv")
    else:
        if args.hispar:
            sub_sites = get_subsites.get_subsites_from_hispar(-1)
            sub_sites.to_csv(f"{str(paths['data'])}/subsites.csv")
        else:
            sub_sites = None

    # Get list of Tor exit nodes
    if args.exit_nodes:
        exit_nodes = args.exit_nodes
    elif args.exit_csv:
        exit_nodes = list(pd.read_csv(args.exit_csv, header=None)[0].values)
    else:
        exit_nodes = get_exit_list()
        if args.e > 0:
            exit_nodes = random.sample(exit_nodes, args.e)
    with open(f"{str(paths['data'])}/exit_nodes.csv", "w") as f:
        for node in exit_nodes:
            f.write(f"{str(node)}\n")

    # print(top_sites)
    # print(exit_nodes)

    # split top_sites into chunks of 5 so that Firefox and Tor are fetched at similar times
    chunk_size = 5

    # Open and save sites in both browsers and for all exit nodes
    for chunk in [top_sites[i:i + chunk_size] for i in range(0, len(top_sites), chunk_size)]:
        for node in exit_nodes:
            download_pages("Tor Browser", chunk, paths, sub_sites, node)
        download_pages("Firefox", chunk, paths, sub_sites, None)


def download_pages(browser, top_sites, paths, sub_sites, exit_node):
    # Set subdirectory for
    browser_data_path = (paths["data"] / browser).resolve()

    # mkdir -p ./data/timestamp/browser
    browser_data_path.mkdir(exist_ok=True)

    tor_process = None

    for top_site in top_sites:

        driver, tor_process = initialize_driver(browser, exit_node, paths, tor_process)
        if sub_sites is not None:
            sites = sub_sites[sub_sites.topsite == top_site]
        else:
            sites = pd.DataFrame([[top_site, top_site, 'landing']], columns=['topsite', 'subsite', 'hash'])
        sites = sites.iloc[::-1]

        for i, row in sites.iterrows():
            site = row.subsite
            hash = row.hash
            try:
                download_page(browser_data_path, driver, exit_node, hash, paths, site, top_site)
            except Exception as err:
                print("Retrying", site, "because of", err)
                # retry once
                try:
                    if tor_process:
                        tor_process.kill()
                    driver.quit()
                    driver, tor_process = initialize_driver(browser, exit_node, paths, tor_process)
                    download_page(browser_data_path, driver, exit_node, hash, paths, site, top_site)
                except Exception as err:
                    print("Aborting", site, "because of", err)
                    pass

        driver.quit()

    print(f"{browser} completed.")

    os.chdir(Path(paths["GeckoDriver"]).resolve().parent)


def initialize_driver(browser, exit_node, paths, tor_process=None):

    if browser == "Firefox":
        options = Options()
        options.headless = False
        pref_dict = get_optimized_prefs()
        for pref_name, pref_val in pref_dict.items():
            options.set_preference(pref_name, pref_val)

        # options.log.level = "trace"  # does not include HTTP requests to websites, only within the Webdriver protocol.
        profile = FirefoxProfile()
        # profile.add_extension(extension='cookies.xpi') # this method of adding extensions to FF doesn't work.
        # profile.set_proxy(proxy.selenium_proxy())  # it's deprecated, but it works and the other way doesn't.
        print(paths["Firefox"] + "/firefox-bin")
        driver = Firefox(firefox_binary=(paths["Firefox"] + "/firefox-bin"), executable_path=paths["GeckoDriver"],
                         firefox_profile=profile, options=options)
        driver.install_addon(paths["base"] + '/har_export_trigger-0.6.1-an+fx.xpi')
        driver.install_addon(paths["base"] + '/cookies.xpi')

    elif browser == "Tor Browser":

        pref_dict = get_optimized_prefs()

        import tempfile
        torrc = {
            'ControlPort': '9251',
            'SOCKSPort': '9250',
            'DataDirectory': tempfile.mkdtemp(),
            'ExitNodes': exit_node
        }
        if tor_process is not None and tor_process.poll() is None:
            # tor_process exists, and has no returncode set, so reuse this one
            print("reuse tor")
            pass
        else:
            if tor_process is not None and tor_process.poll() is not None:
                # tor_process exists, but the returncode attribute is set, so kill it before starting new one
                print("kill and restart tor because of returncode", tor_process.poll())
                tor_process.kill()
            try:
                # start new tor_process
                print("starting new tor")
                tor_process = launch_tbb_tor_with_stem(tbb_path=paths["Tor Browser"], torrc=torrc)
            except OSError as err:
                # Timeout error when starting Tor, likely caused by exit node being unavailable.
                # abort data collection from this exit node
                print(err)
                os.chdir(Path(paths["GeckoDriver"]).resolve().parent)
                # return

        driver = TorBrowserDriver(tbb_path=paths["Tor Browser"], tor_cfg=cm.USE_STEM, socks_port=9250, control_port=9251,
                                  executable_path=paths["GeckoDriver"], pref_dict=pref_dict, headless=False)
        driver.install_addon(paths["base"] + '/har_export_trigger-0.6.1-an+fx.xpi')
        driver.install_addon(paths["base"] + '/cookies.xpi')

    else:
        print(f"[Error] Browser {browser} not known.")

    # Resize window for consistent screenshots
    driver.set_window_position(0, 0)
    driver.set_window_size(1366, 1024)
    driver.set_page_load_timeout(120)
    pyautogui.hotkey('ctrl', 'shift', 'e')  # activate network monitor to allow saving HAR later on.

    return driver, tor_process


def download_page(browser_data_path, driver, exit_node, hash, paths, site, top_site):
    try:
        if not site.startswith("http"):
            driver.get(f"https://{site}/")
        else:
            driver.get(site)
    except Exception as err:
        print("Error downloading page:", err)
        return
    # else:

    if exit_node is not None:
        site_folder = browser_data_path / top_site / str(hash) / exit_node
    else:
        site_folder = browser_data_path / top_site / str(hash)
    site_folder.mkdir(parents=True)

    # Ensure content loads in browser before screenshotting
    time.sleep(3)

    save_data(driver, site, site_folder)

    # identify search boxes
    searchbox = get_search_element(driver)
    if not searchbox:
        # retry once because we have clicked on the parent element in the meantime.
        searchbox = get_search_element(driver)

    if searchbox:
        if exit_node is not None:
            site_folder = browser_data_path / top_site / str(str(hash) + "-search") / exit_node
        else:
            site_folder = browser_data_path / top_site / str(str(hash) + "-search")
        site_folder.mkdir(parents=True)

        try:
            submit_search(searchbox, driver)
            with open(paths["data"] / "search.csv", "a") as f:
                f.write(top_site + "," + str(hash) + "," + driver.current_url + '\n')
            print("searchbox found on", top_site, str(hash), driver.current_url)
            save_data(driver, site, site_folder)
        except (ElementNotInteractableException, NoSuchElementException):
            # if site displays a cookie banner, search box may not be accessible.
            # try clicking on the parent element and re-find the searchbox.
            try:
                searchbox.find_element_by_xpath("./..").click()
                searchbox = get_search_element()
                if searchbox:
                    submit_search(searchbox, driver)
                    with open(paths["data"] / "search.csv", "a") as f:
                        f.write(top_site + "," + str(hash) + "," + driver.current_url + '\n')
                    print("searchbox found on", top_site, str(hash), driver.current_url)
                    save_data(driver, site, site_folder)
            except Exception:
                pass
        except WebDriverException:
            pass


def save_data(driver, site, site_folder, retry=True):
    screenshot_path = f"{str(site_folder)}/screenshot.png"
    driver.get_screenshot_as_file(screenshot_path)
    print(f"Saved screenshot of {site} as {screenshot_path}")

    try:
        page_source_path = f"{str(site_folder)}/page_source.html"
        with open(page_source_path, "w") as f:
            f.write(driver.page_source)
        print(f"Saved {site} as {page_source_path}")
    except Exception:
        if retry:
            driver.refresh()
            save_data(driver, site, site_folder, retry=False)
        return

    # JS to insert to trigger HAR export
    # https://github.com/firefox-devtools/har-export-trigger
    # https://addons.mozilla.org/en-US/firefox/addon/har-export-trigger/
    # pyautogui and sleep idea from
    # https://github.com/theri/web-measurement-tools/blob/master/load/load_url_using_chrome.py#L175
    js = "HAR.triggerExport().then(harLog => { window.foo = harLog; });"
    js2 = "return window.foo;"
    try:
        driver.execute_script(js)
    except JavascriptException as err:
        print(err)
        if retry:
            driver.refresh()
            save_data(driver, site, site_folder, retry=False)
        return

    time.sleep(1)
    har_browser = driver.execute_script(js2)

    har_path = f"{str(site_folder)}/har_browser.json"
    with open(har_path, "w") as f:
        json.dump(har_browser, f)


def submit_search(searchbox, driver):
    def link_has_gone_stale(timeout):
        try:
            # poll the link with an arbitrary call
            searchbox.find_elements_by_id('doesnt-matter')
            return False
        except (StaleElementReferenceException, NoSuchWindowException):
            return True

    searchbox.send_keys("search term")
    searchbox.submit()

    try:
        WebDriverWait(driver, timeout=10).until(link_has_gone_stale)
    except TimeoutException:
        time.sleep(2)
    time.sleep(3)


def test_search_attributes(box):
    names = ['q', 'query', 'querytext', 'search']
    attrs = [box.get_attribute('label'), box.get_attribute('value'), box.get_attribute('id'), box.get_attribute('name')]
    for attr in attrs:
        if attr in names:
            return True
    return False


def test_login_attributes(box):
    # if name is in ['email', 'username', 'identifier'], it's a login box, not a search box
    names = ['email', 'username', 'identifier', 'Email or phone', 'identifierId', 'session[username_or_email]',
             'session_key']
    attrs = [box.get_attribute('label'), box.get_attribute('value'), box.get_attribute('id'), box.get_attribute('name')]
    for attr in attrs:
        if attr in names:
            return True
    if 'mail' in attrs:
        return True
    return False


def get_search_element(driver):
    """ Find search boxes according to heuristics from Singh 2017. """

    # if there is an input with type password nearby, it's a login box, not a search box

    # Heuristic 1: Visible and clickable textbox elements containing search related keywords (q, query, querytext,
    # search) in their element name, id, value, or label are assumed to be search boxes.
    textboxes = driver.find_elements_by_css_selector('input[type="text"]')
    for box in textboxes:
        if box.is_enabled() and box.is_displayed():
            if test_search_attributes(box):
                print("H1")
                return box

    # Heuristic 2: The above heuristic is repeated while considering all input DOM elements
    inputs = driver.find_elements_by_tag_name('input')
    for box in inputs:
        if box.is_enabled() and box.is_displayed():
            if test_search_attributes(box):
                print("H2")
                return box

    # Heuristic 3: If the DOM contains exactly one visible and clickable textbox element,
    # it is assumed to be a search box.
    vis_textboxes = [box for box in textboxes if box.is_enabled() and box.is_displayed()]
    if len(vis_textboxes) == 1:
        if not test_login_attributes(vis_textboxes[0]):
            print("H3")
            return vis_textboxes[0]

    # Heuristic 4: If the DOM contains exactly one visible and clickable input element with a defined max-length,
    # it is assumed to be a search box.
    maxlength_textboxes = driver.find_elements_by_css_selector('input[maxlength]')
    maxlength_textboxes = [box for box in maxlength_textboxes if box.is_enabled() and box.is_displayed()]
    if len(maxlength_textboxes) == 1:
        if not test_login_attributes(maxlength_textboxes[0]):
            print("H4")
            return maxlength_textboxes[0]

    # Heuristic 5: If the DOM contains exactly one visible and clickable input element,
    # it is assumed to be a search box.
    vis_inputs = [box for box in inputs if box.is_enabled() and box.is_displayed()]
    if len(vis_inputs) == 1:
        if not test_login_attributes(vis_inputs[0]):
            print("H5")
            return vis_inputs[0]

    # Heuristic 6: No enabled+visible input element: try clicking on the parent element because this makes the
    # search bar appear in some cases.
    textboxes = driver.find_elements_by_css_selector('input[type="text"]')
    for box in textboxes:
        if not box.is_displayed():
            # try to click on parent element to display the search box
            try:
                box.find_element_by_xpath("./..").click()
                print("H6")
                return
            except Exception:
                pass
    inputs = driver.find_elements_by_tag_name('input')
    for box in inputs:
        if not box.is_displayed():
            # try to click on parent element to display the search box
            try:
                box.find_element_by_xpath("./..").click()
                print("H6")
                return
            except Exception:
                pass


def get_optimized_prefs():
    """
        Disable various features and checks the browser will do on startup.
        Some of these (e.g. disabling the newtab page) are required to prevent
        extraneous data in the proxy.

        Source of prefs:
        * https://support.mozilla.org/en-US/kb/how-stop-firefox-making-automatic-connections
        * https://github.com/pyllyukko/user.js/blob/master/user.js

        Source of (most of) this function: openWPM (configure_firefox.py)
    """
    pref_dict = dict()

    # Startup / Speed
    pref_dict['browser.shell.checkDefaultBrowser'] = False
    pref_dict["browser.slowStartup.notificationDisabled"] = True
    pref_dict["browser.slowStartup.maxSamples"] = 0
    pref_dict["browser.slowStartup.samples"] = 0
    pref_dict['extensions.checkCompatibility.nightly'] = False
    pref_dict['browser.rights.3.shown'] = True
    pref_dict["reader.parse-on-load.enabled"] = False
    pref_dict['browser.pagethumbnails.capturing_disabled'] = True
    pref_dict["browser.uitour.enabled"] = False
    pref_dict["dom.flyweb.enabled"] = False

    # Disable health reports / telemetry / crash reports
    pref_dict["datareporting.policy.dataSubmissionEnabled"] = False
    pref_dict['datareporting.healthreport.uploadEnabled'] = False
    pref_dict["datareporting.healthreport.service.enabled"] = False
    pref_dict['toolkit.telemetry.archive.enabled'] = False
    pref_dict['toolkit.telemetry.enabled'] = False
    pref_dict["toolkit.telemetry.unified"] = False
    pref_dict["breakpad.reportURL"] = ""
    pref_dict["dom.ipc.plugins.reportCrashURL"] = False
    pref_dict["browser.selfsupport.url"] = ""
    pref_dict["browser.tabs.crashReporting.sendReport"] = False
    pref_dict["browser.crashReports.unsubmittedCheck.enabled"] = False
    pref_dict["dom.ipc.plugins.flash.subprocess.crashreporter.enabled"] = False

    # Predictive Actions / Prefetch
    pref_dict["network.predictor.enabled"] = False
    pref_dict["network.dns.disablePrefetch"] = True
    pref_dict["network.prefetch-next"] = False
    pref_dict["browser.search.suggest.enabled"] = False
    pref_dict["network.http.speculative-parallel-limit"] = 0
    pref_dict["keyword.enabled"] = False  # location bar using search
    pref_dict["browser.urlbar.userMadeSearchSuggestionsChoice"] = True
    pref_dict["browser.casting.enabled"] = False

    # Disable pinging Mozilla for geoip
    pref_dict['browser.search.geoip.url'] = ''
    pref_dict["browser.search.countryCode"] = "US"
    pref_dict["browser.search.region"] = "US"

    # Disable pinging Mozilla for geo-specific search
    pref_dict["browser.search.geoSpecificDefaults"] = False
    pref_dict["browser.search.geoSpecificDefaults.url"] = ""

    # Disable auto-updating
    pref_dict["app.update.enabled"] = False  # browser
    pref_dict["app.update.url"] = ""  # browser
    pref_dict["browser.search.update"] = False  # search
    pref_dict["extensions.update.enabled"] = False  # extensions
    pref_dict["extensions.update.autoUpdateDefault"] = False
    pref_dict["extensions.getAddons.cache.enabled"] = False
    pref_dict["lightweightThemes.update.enabled"] = False  # Personas

    # Disable Safebrowsing and other security features
    # that require on remote content
    pref_dict["browser.safebrowsing.phising.enabled"] = False
    pref_dict["browser.safebrowsing.malware.enabled"] = False
    pref_dict["browser.safebrowsing.downloads.enabled"] = False
    pref_dict["browser.safebrowsing.downloads.remote.enabled"] = False
    pref_dict["browser.safebrowsing.blockedURIs.enabled"] = False
    pref_dict[
        "browser.safebrowsing.provider.mozilla.gethashURL"] = ""
    pref_dict[
        "browser.safebrowsing.provider.google.gethashURL"] = ""
    pref_dict[
        "browser.safebrowsing.provider.google4.gethashURL"] = ""
    pref_dict[
        "browser.safebrowsing.provider.mozilla.updateURL"] = ""
    pref_dict[
        "browser.safebrowsing.provider.google.updateURL"] = ""
    pref_dict[
        "browser.safebrowsing.provider.google4.updateURL"] = ""
    pref_dict[
        "browser.safebrowsing.provider.mozilla.lists"] = ""  # TP
    pref_dict[
        "browser.safebrowsing.provider.google.lists"] = ""  # TP
    pref_dict[
        "browser.safebrowsing.provider.google4.lists"] = ""  # TP
    pref_dict["extensions.blocklist.enabled"] = False  # extensions
    pref_dict['security.OCSP.enabled'] = 0

    # Disable Content Decryption Module and OpenH264 related downloads
    pref_dict["media.gmp-manager.url"] = ""
    pref_dict["media.gmp-provider.enabled"] = False
    pref_dict["media.gmp-widevinecdm.enabled"] = False
    pref_dict["media.gmp-widevinecdm.visible"] = False
    pref_dict["media.gmp-gmpopenh264.enabled"] = False

    # Disable Experiments
    pref_dict["experiments.enabled"] = False
    pref_dict["experiments.manifest.uri"] = ""
    pref_dict["experiments.supported"] = False
    pref_dict["experiments.activeExperiment"] = False
    pref_dict["network.allow-experiments"] = False

    # Disable pinging Mozilla for newtab
    pref_dict["browser.newtabpage.directory.ping"] = ""
    pref_dict["browser.newtabpage.directory.source"] = ""
    pref_dict["browser.newtabpage.enabled"] = False
    pref_dict["browser.newtabpage.enhanced"] = False
    pref_dict["browser.newtabpage.introShown"] = True
    pref_dict["browser.aboutHomeSnippets.updateUrl"] = ""

    # Disable Pocket
    pref_dict["extensions.pocket.enabled"] = False

    # Disable Shield
    pref_dict["app.shield.optoutstudies.enabled"] = False
    pref_dict["extensions.shield-recipe-client.enabled"] = False

    # Disable Source Pragams
    # As per https://bugzilla.mozilla.org/show_bug.cgi?id=1628853
    # sourceURL can be used to obfuscate the original origin of
    # a script, we disable it.
    pref_dict["javascript.options.source_pragmas"] = False

    # Enable extensions and disable extension signing
    pref_dict["extensions.experiments.enabled"] = True
    pref_dict["xpinstall.signatures.required"] = False

    # Reduce log level for Torbutton
    pref_dict['extensions.torbutton.loglevel'] = 5
    pref_dict['extensions.torlauncher.loglevel'] = 4

    return pref_dict


def sample_sites(num_sites, start_idx, end_idx, tranco_date='2021-05-11'):
    t = Tranco(cache=True, cache_dir='.tranco')
    date_list = t.list(date=tranco_date)
    sites = date_list.top(end_idx)[start_idx:]
    import random
    samples = random.sample(sites, num_sites)
    with open(f"{num_sites}-samples-from-{start_idx}-{end_idx}.csv", "w") as f:
        for site in samples:
            f.write(f"{site}\n")


def sample_from_hispar(num_sites, exclude_domains):
    import domain_utils
    import hashlib
    # excluded domains: retrive as
    # exc_domains = list(set(tc.f1_df_4.topsite))
    hispar = pd.read_csv('hispar-list-21-01-28', header=None, sep=' ')
    hispar.columns = ['domain_idx', 'page_idx', 'url']
    # page index 0 is the landing page
    hispar['ps_plus_1'] = hispar.url.apply(domain_utils.get_ps_plus_1)
    topurls = set(hispar[hispar.page_idx == 0].url.apply(domain_utils.get_ps_plus_1))
    new_urls = topurls.difference(set(exclude_domains))
    sampled_urls = random.sample(new_urls, num_sites)
    # add subsites with indices 5, 10, 15
    sampled_subsites = []
    for url in sampled_urls:
        dom_idx = hispar[hispar.ps_plus_1 == url].domain_idx.iloc[0]
        for sub_idx in [5, 10, 15]:
            try:
                link = hispar[(hispar.domain_idx == dom_idx) & (hispar.page_idx == sub_idx)].url.values[0]
                sampled_subsites.append([url, link, hashlib.sha1(link.encode()).hexdigest()])
            except IndexError:  # when the subsite index doesn't exist
                print("no subsite:", url, sub_idx)
                pass
        sampled_subsites.append([url, url, 'landing'])
    with open(f"2021-10-09-{num_sites}-samples-from-hispar.csv", "w") as f:
        for site in sampled_urls:
            f.write(f"{site}\n")
    subsites = pd.DataFrame(sampled_subsites, columns=['topsite', 'subsite', 'hash'])
    csvname = f"subsites-2021-10-09-{num_sites}-samples-from-hispar.csv-3.csv"
    subsites.to_csv(csvname)
    return sampled_urls + sampled_subsites


def convert_hispar():
    import domain_utils
    import hashlib
    hispar = pd.read_csv('hispar-list-21-01-28', header=None, sep=' ')
    hispar.columns = ['domain_idx', 'page_idx', 'url']
    # page index 0 is the landing page
    hispar['ps_plus_1'] = hispar.url.apply(domain_utils.get_ps_plus_1)
    topurls = set(hispar[hispar.page_idx == 0].url.apply(domain_utils.get_ps_plus_1))
    subsites = []
    for url in topurls:
        subsites.append([url, url, 'landing'])
        dom_idx = hispar[hispar.ps_plus_1 == url].domain_idx.iloc[0]
        links = hispar[(hispar.domain_idx == dom_idx)].url
        for link in links:
            subsites.append([url, link, hashlib.sha1(link.encode()).hexdigest()])
    with open(f"2021-10-20-hispar-topsites.csv", "w") as f:
        for site in topurls:
            f.write(f"{site}\n")
    subsites = pd.DataFrame(subsites, columns=['topsite', 'subsite', 'hash'])
    subsites.to_csv('2021-10-20-hispar-subsites.csv')


def get_exit_list():
    # get list of exit nodes
    listname = '2021-10-26-exit-node-list.csv'

    if os.path.exists(listname):
        exit_nodes = list(pd.read_csv(listname, header=None)[0])
        return exit_nodes

    resp = requests.get('https://check.torproject.org/torbulkexitlist')
    exit_nodes = [node.decode() for node in resp.content.split()]
    ok_exit_nodes = []
    node_properties = []

    basepath = Path(__file__).resolve().parent / "exit_nodes"
    basepath.mkdir(exist_ok=True)

    # get properties of exit nodes (exit policy, exit probability)
    for node in exit_nodes:
        # get exit node details from onionoo
        # save results as json so we don't have to make network requests all the time
        if os.path.exists(basepath / f"{node}.json"):
            resp = json.load(open(f"exit_nodes/{node}.json", "r"))
        else:
            req = f'https://onionoo.torproject.org/details?limit=2&search={node}&fields=' \
                  f'exit_probability,exit_policy_summary,running,,first_seen,advertised_bandwidth,country,as'
            resp = requests.get(req)
            if resp.status_code == 200:
                resp = json.loads(resp.content.decode())
                json.dump(resp, open(f"exit_nodes/{node}.json", "w"))

        if len(resp['relays']) == 0:
            continue
        # exit_prob = resp['relays'][0]['exit_probability']
        acc_ports = []
        rej_ports = []
        node_accepts_ok = False
        if 'accept' in resp['relays'][0]['exit_policy_summary'].keys():
            for ports in resp['relays'][0]['exit_policy_summary']['accept']:
                split_ports = ports.split('-')
                if len(split_ports) == 2:
                    acc_ports.extend(list(range(int(split_ports[0]), int(split_ports[1]) + 1)))
                else:
                    acc_ports.append(int(split_ports[0]))
            if 80 in acc_ports and 443 in acc_ports and resp['relays'][0]['running']:
                # exit policy allows ports 80 and 443
                node_accepts_ok = True

        elif 'reject' in resp['relays'][0]['exit_policy_summary'].keys():
            for ports in resp['relays'][0]['exit_policy_summary']['reject']:
                split_ports = ports.split('-')
                if len(split_ports) == 2:
                    rej_ports.extend(list(range(int(split_ports[0]), int(split_ports[1]) + 1)))
                else:
                    rej_ports.append(int(split_ports[0]))
            if 80 not in rej_ports and 443 not in rej_ports and resp['relays'][0]['running']:
                # exit policy allows ports 80 and 443
                node_accepts_ok = True

        if node_accepts_ok:
            ok_exit_nodes.append(node)
            try:
                node_properties.append([node, resp['relays'][0]['country'], resp['relays'][0]['as'],
                                        resp['relays'][0]['exit_probability']])
            except KeyError as err:
                print(node, err)

    with open(listname, "w") as f:
        for node in ok_exit_nodes:
            f.write(f"{node}\n")

    properties_df = pd.DataFrame(node_properties)
    properties_df.to_csv("2021-10-26_exit-node-properties.csv")

    return ok_exit_nodes


def get_exit_sample():
    # Samples from exit nodes by country so that each country is represented at least once, more often depending
    # on the number of exit nodes. Ensure that each AS is sampled at most once.
    df = pd.read_csv("2021-10-26_exit-node-properties.csv", index_col=0)
    df.columns = ['ip', 'country', 'asname', 'prob']
    sample_df = pd.DataFrame()
    for country, idx in df.groupby('country').groups.items():
        country_df = df.iloc[idx]
        sample_num = 1
        if len(idx) >= 100:
            sample_num = 6
        elif len(idx) >= 50:
            sample_num = 5
        elif len(idx) >= 25:
            sample_num = 4
        elif len(idx) >= 15:
            sample_num = 3
        elif len(idx) >= 5:
            sample_num = 2

        unique_as = set(country_df.asname)
        if len(unique_as) < sample_num:
            print(f"not enough unique AS for country {country}: have {len(unique_as)}, need {sample_num}")
        sample_num = min(sample_num, len(unique_as))
        sample_as = random.sample(unique_as, sample_num)
        for asname in sample_as:
            # choose one node from the sampled as
            sample_idx = random.choice(list(country_df[country_df.asname == asname].index))
            sample_df = sample_df.append(df.iloc[sample_idx])
    sample_df.to_csv("2021-10-26_exit-node-sample-df.csv")

    # save sets of 10 exit nodes for easier distribution on boinc
    nodes = list(sample_df.ip)
    chunk_size = 10
    for j, chunk in enumerate([nodes[i:i + chunk_size] for i in range(0, len(nodes), chunk_size)]):
        pd.DataFrame(chunk).to_csv(f"2021-10-26_exit_set_{j}.csv", header=None, index=None)


def plot_hispar_ranks():
    hispar = pd.read_csv("2021-10-20-hispar-topsites.csv", header=None)
    t = Tranco(cache=True, cache_dir='.tranco')
    tranco_list = t.list(date='2021-05-11')

    ranks = []
    for url in list(hispar[0]):
        ranks.append(tranco_list.rank(url))
    ranks = sorted(ranks)
    import seaborn as sns
    import matplotlib.pyplot as plt
    g = sns.histplot(ranks, log_scale=True)
    g.set(ylabel="Number of sites (Hispar)", xlabel="Tranco rank")
    plt.savefig('hispar-ranks.png')


if __name__ == '__main__':
    main()

