import os
import sys

import pandas as pd
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QLabel, QWidget, QGridLayout, QPushButton
from PyQt5.QtGui import QFont
import analyze


def set_block_reason(df, idx, reason):
    df.loc[idx, 'lookalike'] = reason


class MyButton(QPushButton):
    def __init__(self, label):
        QPushButton.__init__(self, label)
        size_policy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        self.setSizePolicy(size_policy)
        self.setMinimumWidth(500)


def label_data_app(data, i, screenshots):
    app = QApplication([])
    window = QWidget()
    layout = QGridLayout()

    title_label = QLabel('Compared with the Firefox version, what does the Tor version show?')
    title_label.setFont(QFont('Arial', 12))
    # arguments are: widget, row, column, rowSpan, columnSpan, alignment
    layout.addWidget(title_label, 0, 0, 1, 3, Qt.AlignCenter)

    layout.addWidget(QLabel('Firefox'), 1, 0, Qt.AlignCenter)
    layout.addWidget(QLabel('Tor Browser'), 2, 0, Qt.AlignCenter)

    ff_pixlabel = analyze.MyLabelPixmap(screenshots[0])
    layout.addWidget(ff_pixlabel, 1, 1)

    tor_pixlabel = analyze.MyLabelPixmap(screenshots[1])
    layout.addWidget(tor_pixlabel, 2, 1)

    block_button = MyButton('a block page')
    captcha_button = MyButton('a captcha')
    missing_button = MyButton('missing elements (such as login buttons or search functions)')
    slow_button = MyButton('missing elements due to slow loading/timeouts')
    cookie_button = MyButton('a cookie banner, where Firefox shows no cookie banner')
    nocookie_button = MyButton('no cookie banner, where Firefox shows one')
    other_button = MyButton('other substantial difference, e.g., localization')
    same_button = MyButton('no substantial difference')
    skip_button = MyButton('skip (do not label)')
    exit_button = QPushButton('stop labeling,\nsave progress')

    def on_block_button_clicked():
        set_block_reason(data, i, 'block')
        window.close()

    def on_captcha_button_clicked():
        set_block_reason(data, i, 'captcha')
        window.close()

    def on_missing_button_clicked():
        set_block_reason(data, i, 'missing')
        window.close()

    def on_slow_button_clicked():
        set_block_reason(data, i, 'slow')
        window.close()

    def on_cookie_button_clicked():
        set_block_reason(data, i, 'cookie')
        window.close()

    def on_nocookie_button_clicked():
        set_block_reason(data, i, 'nocookie')
        window.close()

    def on_other_button_clicked():
        set_block_reason(data, i, 'other')
        window.close()

    def on_same_button_clicked():
        set_block_reason(data, i, 'same')
        window.close()

    def on_skip_button_clicked():
        window.close()

    def on_exit_button_clicked():
        data.to_csv(id + ".csv")
        sys.exit()

    block_button.clicked.connect(on_block_button_clicked)
    captcha_button.clicked.connect(on_captcha_button_clicked)
    missing_button.clicked.connect(on_missing_button_clicked)
    slow_button.clicked.connect(on_slow_button_clicked)
    cookie_button.clicked.connect(on_cookie_button_clicked)
    nocookie_button.clicked.connect(on_nocookie_button_clicked)
    other_button.clicked.connect(on_other_button_clicked)
    same_button.clicked.connect(on_same_button_clicked)
    skip_button.clicked.connect(on_skip_button_clicked)
    exit_button.clicked.connect(on_exit_button_clicked)

    layout.addWidget(exit_button, 3, 0)

    button_layout = QGridLayout()
    # layout.addWidget(block_button, 3, 1)
    # layout.addWidget(captcha_button, 4, 1)
    # layout.addWidget(missing_button, 5, 1)
    # layout.addWidget(slow_button, 6, 1)
    # layout.addWidget(cookie_button, 7, 1)
    # layout.addWidget(nocookie_button, 8, 1)
    # layout.addWidget(same_button, 9, 1)
    button_layout.addWidget(block_button, 0, 0)
    button_layout.addWidget(captcha_button, 1, 0)
    button_layout.addWidget(missing_button, 2, 0)
    button_layout.addWidget(slow_button, 3, 0)
    button_layout.addWidget(cookie_button, 4, 0)
    button_layout.addWidget(nocookie_button, 5, 0)
    button_layout.addWidget(other_button, 6, 0)
    button_layout.addWidget(same_button, 7, 0)
    button_layout.addWidget(skip_button, 8, 0)
    layout.addLayout(button_layout, 3, 1)

    app.setQuitOnLastWindowClosed(True)
    window.setLayout(layout)
    window.resize(900, window.height())
    window.show()
    app.exec()


if __name__ == "__main__":
    id = '2021-08-30_151405_ground_truth_data'
    # id = '2021-09-12_123705_ground_truth_data2'
    # id = '2021-09-13_120141_ground_truth_data3'
    id = '2021-10-10_023311_ground_truth_data4'
    if os.path.exists(id + ".csv"):
        data = pd.read_csv(id + ".csv", index_col=0)
    else:
        data = analyze.read_data(id)
        # data = analyze.compute_features(data)
        # data = analyze.compute_diff_features(data)

    for i, row in data.iterrows():
        if row.browser == 'Firefox':
            continue
        # find the equivalent Firefox row
        if 'lookalike' in row.keys() and not pd.isna(row.lookalike):
            # this row has already been labeled
            # print(row.index, row.lookalike)
            continue
        ffrow = data[(data.browser == 'Firefox') & (data.topsite == row.topsite) & (data.subsite == row.subsite)]
        if not pd.isna(ffrow.iloc[0].screenshot) and not pd.isna(row.screenshot):
            screenshots = [ffrow.iloc[0].screenshot, row.screenshot]
            label_data_app(data, i, screenshots)

    # save with 'labeled' suffix once labeling is complete
    data.to_csv(id + "-labeled.csv")
