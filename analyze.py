import datetime
import difflib
import glob
import itertools
import json
import os
from pathlib import Path

import datasketch  # https://github.com/ekzhu/datasketch
import findcdn  # pip install git+https://github.com/cisagov/findcdn.git
import imagehash
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import scipy.stats as st
from PIL import Image
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap, QFont
from PyQt5.QtWidgets import QApplication, QLabel, QWidget, QGridLayout, QPushButton
from joblib import load, dump
from sklearn import metrics
from tqdm import tqdm
from tranco import Tranco


def preprocess_text(line):
    """ convert line to lower-case, remove HTML tags, remove any characters that are not a-z 0-9 or space. """
    from gensim.parsing import preprocessing
    filters = [lambda x: x.lower(), preprocessing.strip_tags, preprocessing.strip_punctuation,
               preprocessing.strip_non_alphanum, preprocessing.strip_multiple_whitespaces]
    newline = preprocessing.preprocess_string(line, filters)
    newline = " ".join(newline)
    newline = preprocessing.strip_multiple_whitespaces(newline)
    return newline


def cohens_d(c0, c1):
    # Returns Cohen's d effect size.
    # Small Effect Size: d=0.20
    # Medium Effect Size: d=0.50
    # Large Effect Size: d=0.80
    # see http://www.statisticslectures.com/topics/effectsizeindependentsamplest/
    # https://stackoverflow.com/a/21532472
    from statistics import mean, stdev
    from math import sqrt
    return abs((mean(c0) - mean(c1)) / (sqrt((stdev(c0) ** 2 + stdev(c1) ** 2) / 2)))


def set_block_reason(data, i, reason):
    data.loc[i, 'lookalike'] = reason
    if reason:
        data.loc[i, "singh"] = 'ok (m)'
    else:
        data.loc[i, "singh"] = 'blocked (m)'
    # sites_data[site]["block_reason"]=reason


class MyLabelPixmap(QLabel):
    def __init__(self, imagepath):
        QLabel.__init__(self)
        size_policy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        size_policy.setHeightForWidth(True)
        # Tor screenshots are less wide than Firefox screenshots; use reference size for resizing to display them
        # at same size.
        self.ref_pixmap = QPixmap(1366, 700)
        self.setSizePolicy(size_policy)
        self.pixmap = QPixmap(imagepath)
        self.setPixmap(self.pixmap)
        self.installEventFilter(self)
        self.resize(800, 410)
        self.setMinimumSize(800, 410)

    def eventFilter(self, source, event):
        if source is self and event.type() == QtCore.QEvent.Resize:
            temp_pixmap = self.ref_pixmap.scaled(self.size(), Qt.KeepAspectRatio, Qt.FastTransformation)
            # self.setPixmap(self.pixmap.scaled(self.size(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
            self.setPixmap(self.pixmap.scaledToWidth(temp_pixmap.width(), Qt.SmoothTransformation))
        return super(QWidget, self).eventFilter(source, event)

    def sizeHint(self):
        return QtCore.QSize(800, 410)

    def heightForWidth(self, width):
        return self.pixmap.height() * width / self.pixmap.width()


def manual_check_qt(data, i, screenshots):
    app = QApplication([])
    window = QWidget()
    layout = QGridLayout()

    title_label = QLabel('Is the site displayed correctly in Tor Browser?')
    title_label.setFont(QFont('Arial', 12))
    layout.addWidget(title_label, 0, 0, 1, 2, Qt.AlignCenter)

    layout.addWidget(QLabel('Firefox'), 1, 0, Qt.AlignCenter)
    layout.addWidget(QLabel('Tor Browser'), 1, 1, Qt.AlignCenter)

    yes_button = QPushButton('Yes')
    no_button = QPushButton('No')

    def on_yes_button_clicked():
        set_block_reason(data, i, True)
        window.close()

    def on_no_button_clicked():
        set_block_reason(data, i, False)
        window.close()

    yes_button.clicked.connect(on_yes_button_clicked)
    no_button.clicked.connect(on_no_button_clicked)

    ff_pixlabel = MyLabelPixmap(screenshots[0])
    layout.addWidget(ff_pixlabel, 2, 0)

    tor_pixlabel = MyLabelPixmap(screenshots[1])
    layout.addWidget(tor_pixlabel, 2, 1)

    layout.addWidget(yes_button, 3, 0)
    layout.addWidget(no_button, 3, 1)
    window.setLayout(layout)
    window.show()
    app.setQuitOnLastWindowClosed(True)
    app.exec()


def read_data(id):
    datapath = Path(__file__).resolve().parent / "data" / id

    sites = pd.read_csv(datapath / "sites.csv", index_col=0, header=None)
    sub_sites = pd.read_csv(datapath / "subsites.csv", index_col=0)
    if os.path.exists(datapath / "search.csv"):
        search = pd.read_csv(datapath / "search.csv", header=None, on_bad_lines='warn').drop_duplicates()
    else:
        search = pd.DataFrame()
    exit_nodes = pd.read_csv(datapath / "exit_nodes.csv", header=None)

    all_sites = []
    browser = "Firefox"
    for top_site in sites[1]:
        rows = sub_sites[sub_sites.topsite == top_site]
        for i, row in rows.iterrows():
            hash = row.hash
            site = row.subsite
            relpath = Path(browser) / top_site / str(hash)
            screenshot, source, har = get_resolved_paths(datapath, relpath)
            all_sites.append([browser, top_site, hash, None, screenshot, source, har, relpath])
    for i, row in search.iterrows():
        relpath = Path(browser) / row[0] / str(str(row[1]) + "-search")
        screenshot, source, har = get_resolved_paths(datapath, relpath)
        all_sites.append([browser, row[0], str(row[1]) + "-search", None, screenshot, source, har, relpath])

    browser = "Tor Browser"
    for top_site in sites[1]:
        rows = sub_sites[sub_sites.topsite == top_site]
        for i, row in rows.iterrows():
            hash = row.hash
            site = row.subsite
            for node in exit_nodes[0]:
                relpath = Path(browser) / top_site / str(hash) / node
                screenshot, source, har = get_resolved_paths(datapath, relpath)
                all_sites.append([browser, top_site, hash, node, screenshot, source, har, relpath])
    for i, row in search.iterrows():
        for node in exit_nodes[0]:
            relpath = Path(browser) / row[0] / str(str(row[1]) + "-search") / node
            screenshot, source, har = get_resolved_paths(datapath, relpath)
            all_sites.append([browser, row[0], str(row[1]) + "-search", node, screenshot, source, har, relpath])

    data = pd.DataFrame(all_sites,
                        columns=['browser', 'topsite', 'subsite', 'exit', 'screenshot', 'source', 'har', 'relpath'])
    return data


def get_resolved_paths(datapath, relpath):
    path = Path(datapath / relpath)
    if path.is_dir():
        screen_path = str((path / "screenshot.png").resolve())
        source_path = str((path / "page_source.html").resolve())
        har_path = str((path / "har_browser.json").resolve())
        if not os.path.exists(screen_path):
            screen_path = None
        if not os.path.exists(source_path):
            source_path = None
        if not os.path.exists(har_path):
            har_path = None
        return screen_path, source_path, har_path
    else:
        return None, None, None


def get_phash(data, hash_size=32, highfreq_factor=10):
    name = 'phash_' + str(hash_size) + "_" + str(highfreq_factor)
    for i, row in data.iterrows():
        if row.screenshot and not pd.isna(row.screenshot):
            data.loc[i, name] = imagehash.phash(Image.open(row.screenshot), hash_size=hash_size,
                                                highfreq_factor=highfreq_factor)
    return data


def get_whash(data, hash_size=32):
    name = 'whash_' + str(hash_size)
    for i, row in data.iterrows():
        if row.screenshot and not pd.isna(row.screenshot):
            data.loc[i, name] = imagehash.whash(Image.open(row.screenshot), hash_size=hash_size)
    return data


def get_source_size(data):
    for i, row in data.iterrows():
        if row.source and not pd.isna(row.source):
            data.loc[i, "source_size"] = Path(row.source).stat().st_size
    return data


def get_status_codes(data):
    # get number of HTTP response status codes and number of 200-level status codes
    for i, row in data.iterrows():
        if row.har is None or pd.isna(row.har):
            continue
        with open(row.har, 'r') as f:
            har = json.load(f)

        if har is None:
            continue

        num_responses = 0
        num_2xx_responses = 0
        num_3xx_responses = 0
        num_5xx_responses = 0
        first_status = None
        if isinstance(har, dict) and 'entries' in har.keys():
            for entry in har['entries']:
                num_responses += 1
                status = entry['response']['status']
                if not first_status:
                    first_status = status
                if 200 <= status < 300:
                    num_2xx_responses += 1
                if 300 <= status < 400:
                    num_3xx_responses += 1
                if 500 <= status < 600:
                    num_5xx_responses += 1
                # if status >= 300:
                #     print(status)
            # print(row.topsite, row.subsite, num_responses, num_2xx_responses)
            data.loc[i, 'num_responses'] = num_responses
            data.loc[i, 'num_2xx'] = num_2xx_responses
            data.loc[i, 'num_3xx'] = num_3xx_responses
            data.loc[i, 'num_5xx'] = num_5xx_responses
            data.loc[i, 'first_status'] = first_status
    return data


def load_node_properties(node):
    import requests
    basepath = Path(__file__).resolve().parent / "exit_nodes"
    basepath.mkdir(exist_ok=True)

    # get properties of exit nodes (exit policy, exit probability) from onionoo
    # save results as json so we don't have to make network requests all the time
    if os.path.exists(basepath / f"{node}.json"):
        resp = json.load(open(f"exit_nodes/{node}.json", "r"))
    else:
        req = f'https://onionoo.torproject.org/details?limit=2&search={node}&fields=' \
              f'exit_probability,exit_policy_summary,running,first_seen,advertised_bandwidth,country,as'
        resp = requests.get(req)
        if resp.status_code == 200:
            resp = json.loads(resp.content.decode())
            json.dump(resp, open(f"exit_nodes/{node}.json", "w"))
    return resp


def get_exit_prob(node):
    resp = load_node_properties(node)
    try:
        exit_prob = resp['relays'][0]['exit_probability']
        return exit_prob
    except (IndexError, KeyError):
        df = pd.read_csv("2021-10-26_exit-node-properties.csv", index_col=0)
        df.columns = ['ip', 'country', 'asname', 'prob']
        return df[df.ip == node].prob.values[0]
        # print("exit prob: index/key error for", node)
        # return 0


def get_age(node):
    resp = load_node_properties(node)
    try:
        first_seen = resp['relays'][0]['first_seen']
    except IndexError:
        # print("age: index error for", node)
        return 0
    delta = datetime.datetime.now() - datetime.datetime.fromisoformat(first_seen)
    return delta.total_seconds()


def get_bandwidth(node):
    resp = load_node_properties(node)
    try:
        return resp['relays'][0]['advertised_bandwidth']
    except IndexError:
        # print("bandwidth: index error for", node)
        return 0


def get_num_ports(node):
    resp = load_node_properties(node)
    if len(resp['relays']) == 0:
        # print("ports: no relays for", node)
        return 0
    if "accept" in resp['relays'][0]['exit_policy_summary'].keys():
        acc_ports = []
        accepts = resp['relays'][0]['exit_policy_summary']['accept']
        for ports in accepts:
            split_ports = ports.split('-')
            if len(split_ports) == 2:
                acc_ports.extend(list(range(int(split_ports[0]), int(split_ports[1]) + 1)))
            else:
                acc_ports.append(int(split_ports[0]))
        return len(acc_ports)

    if "reject" in resp['relays'][0]['exit_policy_summary'].keys():
        max_ports = 65535
        rej_ports = []
        rejects = resp['relays'][0]['exit_policy_summary']['reject']
        for ports in rejects:
            split_ports = ports.split('-')
            if len(split_ports) == 2:
                rej_ports.extend(list(range(int(split_ports[0]), int(split_ports[1]) + 1)))
            else:
                rej_ports.append(int(split_ports[0]))

        return max_ports - len(rej_ports)


def get_country(node):
    df = pd.read_csv("2021-10-26_exit-node-properties.csv", index_col=0)
    df.columns = ['ip', 'country', 'asname', 'prob']
    return df[df.ip == node].country.values[0]


def get_site_categories_mcafee(urls, increment=20):
    """
    Categorize URLs using McAfee service.
    :param urls: list of all URLs to be categorized
    :param increment: categorize increment URLs at a time to reduce runtime and load on McAfee service.
    :return: Returns DataFrame with one entry per url/category combination.
    """
    import random as rd
    import time
    from numpy import random
    from selenium.webdriver.support.ui import Select
    from bs4 import BeautifulSoup

    saved_wd = os.getcwd()
    domain_cats = []
    remaining_urls = urls
    tor_process = None
    while len(remaining_urls) > 0:
        increment = min(len(remaining_urls), increment)
        mcafee_cats = {}
        link = 'https://www.trustedsource.org/'

        # profile = webdriver.FirefoxProfile()
        # profile.DEFAULT_PREFERENCES['frozen']['javascript.enabled'] = True
        # profile.set_preference("app.update.auto", False)
        # profile.set_preference("app.update.enabled", False)
        # profile.update_preferences()
        # options = Options()

        from tbselenium.tbdriver import TorBrowserDriver
        from tbselenium.utils import launch_tbb_tor_with_stem
        import tbselenium.common as cm

        tbb_dir = "/home/isabel/software/tor-browser_en-US"
        try:
            tor_process = launch_tbb_tor_with_stem(tbb_path=tbb_dir)
        except OSError:
            # may happen if tor process has already been launched. try to continue.
            pass
        driver = TorBrowserDriver(tbb_dir, tor_cfg=cm.USE_STEM)

        # driver = webdriver.Firefox(options=options, firefox_profile=profile,
        #                            executable_path=os.path.join(root_dir, 'geckodriver'))
        driver.get(link)

        for url in rd.sample(remaining_urls, increment):
            driver.find_element_by_name("url").clear()
            driver.find_element_by_name("url").send_keys(url)
            droplist = Select(driver.find_element_by_name("product"))
            droplist.select_by_value('01-ts')
            driver.find_element_by_class_name("button").submit()
            time.sleep(9 + random.random())
            response = driver.page_source
            page = BeautifulSoup(response, features="lxml")
            result_table = page.find_all("table", {"class": "result-table"})
            mcafee_cats[url] = result_table
            time.sleep(random.randint(1, 4) * random.random())

        driver.quit()

        # extract category names from html and store in database (categorytbl)
        for url, res in mcafee_cats.items():
            # res[0] is a BeautifulSoup Tag object
            try:
                cats = res[0].find("td", text="Categorized URL").find_next_sibling("td").text
            except AttributeError:
                # if URL is uncategorized
                cats = "- Uncategorized"
            for cat in cats.split("- "):
                if len(cat) > 0:
                    domain_cats.append((url, cat))
            remaining_urls.remove(url)

        print("remaining:", len(remaining_urls))

    if tor_process is not None:
        tor_process.kill()
    os.chdir(saved_wd)
    return pd.DataFrame(domain_cats, columns=['domain', 'category'])


def compute_features(df):
    df = get_phash(df, hash_size=8, highfreq_factor=4)
    df = get_phash(df, hash_size=16, highfreq_factor=8)
    df = get_phash(df, hash_size=32, highfreq_factor=10)
    df = get_phash(df, hash_size=32, highfreq_factor=16)
    df = get_whash(df, hash_size=8)
    df = get_whash(df, hash_size=16)
    df = get_whash(df, hash_size=32)

    df = get_source_size(df)
    df = get_status_codes(df)

    df = df.drop_duplicates()

    from bs4 import BeautifulSoup
    from urllib.parse import urlparse, urlunparse
    import json
    for i, row in df.iterrows():
        # features of the source HTML: numbers for all tags
        if row.source and not pd.isna(row.source):
            source = "\n".join(open(row.source).readlines())
            page = BeautifulSoup(source, features="lxml")
            df.loc[i, 'text'] = preprocess_text(page.getText(separator=' '))
            tags = set(tag.name for tag in page.find_all())
            num_tags = len(page.findAll())
            for tag in tags:
                df.loc[i, 'tag_' + tag] = len(page.findAll(tag))
            df.loc[i, 'num_tags'] = num_tags

            # sources for included scripts (not including inline scripts which don't have the src attribute)
            # from entire corpus, can build up a vocabulary of scripts and one-hot encode which scripts are present for each site
            script_sources = []
            script_domains = []
            script_paths = []
            for s in page.find_all('script'):
                if 'src' in s.attrs.keys():
                    src = s.attrs['src']
                    u = urlparse(src)
                    src_url = urlunparse((u.scheme, u.netloc, u.path, '', '', ''))
                    if "harapi.js" in u.path:
                        # script inserted by HAR extension. skip because it will be present on each site.
                        continue
                    script_sources.append(src_url)
                    script_domains.append(u.netloc)
                    script_paths.append(u.path)
            df.loc[i, 'script_sources'] = str(script_sources)
            df.loc[i, 'script_domains'] = str(script_domains)
            df.loc[i, 'script_paths'] = str(script_paths)

        # features of the HAR: requests/responses
        if row.har and not pd.isna(row.har):
            with open(row.har, 'r') as f:
                har = json.load(f)
            # numbers for all received status codes
            statuses = dict()
            # number of requests/responses that include/set at least one cookie
            num_cookie_reqs = 0
            num_cookie_resps = 0
            if har and isinstance(har, dict) and 'entries' in har.keys():
                for entry in har['entries']:
                    status = entry['response']['status']
                    if status not in statuses:
                        statuses[status] = 1
                    else:
                        statuses[status] += 1

                    if len(entry['request']['cookies']) > 0:
                        num_cookie_reqs += 1
                    if len(entry['response']['cookies']) > 0:
                        num_cookie_resps += 1

                for key, val in statuses.items():
                    df.loc[i, 'status_' + str(key)] = val
                # number of requests sent
                num_requests = len(har['entries'])
                df.loc[i, 'num_requests'] = num_requests
            df.loc[i, 'num_cookie_reqs'] = num_cookie_reqs
            df.loc[i, 'num_cookie_resps'] = num_cookie_resps

    for group in df.groupby(by=['topsite', 'subsite']):
        # group[0] is a tuple with the topsite and subsite
        site = " ".join(group[0])
        # group[1] is a data frame with the data for the group
        firefox = group[1][group[1].browser == 'Firefox']
        tor = group[1][group[1].browser == 'Tor Browser']
        for i, row in tor.iterrows():
            # unified diff
            if not pd.isna(firefox.source.values[0]) and not pd.isna(row.source):
                firefox_source = open(firefox.source.values[0]).readlines()
                tor_source = open(row.source).readlines()

                changes = 0
                for line in difflib.unified_diff(firefox_source, tor_source, n=0):
                    if line.startswith("+ ") or line.startswith("- "):
                        changes = changes + 1
            else:
                changes = np.nan
            df.loc[i, 'unif_diff'] = changes

    return df


def compute_diff_features(df, only_first=False):
    hash_names = ['phash_8_4', 'phash_16_8', 'phash_32_10', 'phash_32_16', 'whash_8', 'whash_16', 'whash_32']
    for name in hash_names:
        df.loc[:, name + "_diff"] = np.nan

    df.loc[:, "source_diff"] = np.nan
    df.loc[:, "numresp_diff"] = np.nan
    df.loc[:, "perc2xx_diff"] = np.nan
    df.loc[:, "perc3xx_diff"] = np.nan
    df.loc[:, "perc5xx_diff"] = np.nan
    for group in tqdm(df.groupby(by=['topsite', 'subsite']), desc='diff features (1)'):
        # group[0] is a tuple with the topsite and subsite
        site = " ".join(group[0])
        # group[1] is a data frame with the data for the group
        firefox = group[1][group[1].browser == 'Firefox']
        tor = group[1][group[1].browser == 'Tor Browser']
        for i, row in tor.iterrows():

            # difference in phash values between Firefox and Tor
            for name in hash_names:
                if not pd.isna(row[name]) and not pd.isna(firefox[name].values[0]):
                    phash_diff = abs(firefox[name].values[0] - row[name])
                else:
                    phash_diff = np.nan
                df.loc[i, name + '_diff'] = phash_diff

            # difference in size of the HTML source between Firefox and Tor
            if not pd.isna(row.source_size) and not pd.isna(firefox.source_size.values[0]):
                source_diff = abs(firefox.source_size.values[0] - row.source_size)
                source_perc_diff = source_diff / firefox.source_size.values[0]
            else:
                source_diff = np.nan
                source_perc_diff = np.nan
            df.loc[i, 'source_diff'] = source_diff
            df.loc[i, 'source_perc_diff'] = source_perc_diff

            # difference in number of responses between Firefox and Tor
            if not pd.isna(row.num_responses) and not pd.isna(firefox.num_responses.values[0]):
                numresp_diff = abs(firefox.num_responses.values[0] - row.num_responses)
            else:
                numresp_diff = np.nan
            df.loc[i, 'numresp_diff'] = numresp_diff

            # difference in percentage of 2xx/3xx/5xx response codes between Firefox and Tor
            if not pd.isna(row.num_2xx) and not pd.isna(firefox.num_2xx.values[0]) and row.num_responses > 0:
                ff_perc = firefox.num_2xx.values[0] / firefox.num_responses.values[0]
                tor_perc = row.num_2xx / row.num_responses
                perc2xx_diff = abs(ff_perc - tor_perc)
            else:
                perc2xx_diff = np.nan
            df.loc[i, 'perc2xx_diff'] = perc2xx_diff

            if not pd.isna(row.num_3xx) and not pd.isna(firefox.num_3xx.values[0]) and row.num_responses > 0:
                ff_perc = firefox.num_3xx.values[0] / firefox.num_responses.values[0]
                tor_perc = row.num_3xx / row.num_responses
                perc3xx_diff = abs(ff_perc - tor_perc)
            else:
                perc3xx_diff = np.nan
            df.loc[i, 'perc3xx_diff'] = perc3xx_diff

            if not pd.isna(row.num_5xx) and not pd.isna(firefox.num_5xx.values[0]) and row.num_responses > 0:
                ff_perc = firefox.num_5xx.values[0] / firefox.num_responses.values[0]
                tor_perc = row.num_5xx / row.num_responses
                perc5xx_diff = abs(ff_perc - tor_perc)
            else:
                perc5xx_diff = np.nan
            df.loc[i, 'perc5xx_diff'] = perc5xx_diff

    # tag and status columns should be have default value of 0 instead of nan
    cols = [c for c in df.columns if c.startswith('tag_')]
    cols.extend([c for c in df.columns if c.startswith('status_')])
    for col in cols:
        df.loc[pd.isna(df[col]), col] = 0
    cols.extend(['num_requests', 'num_cookie_reqs', 'num_cookie_resps', 'num_tags'])

    if only_first:
        return df

    # compute diffs between firefox and tor
    for group in tqdm(df.groupby(by=['topsite', 'subsite']), desc='diff features (2)'):
        # group[0] is a tuple with the topsite and subsite
        site = " ".join(group[0])
        # group[1] is a data frame with the data for the group
        firefox = group[1][group[1].browser == 'Firefox']
        tor = group[1][group[1].browser == 'Tor Browser']
        for i, row in tor.iterrows():
            for col in cols:
                df.loc[i, col + '_diff'] = firefox[col].values[0] - row[col]

            if not pd.isna(row.script_sources) and not pd.isna(firefox['script_sources'].values[0]):
                tor_scripts = set(eval(row['script_sources']))
                ff_scripts = set(eval(firefox['script_sources'].values[0]))
                diff = tor_scripts.difference(ff_scripts)
                df.loc[i, 'script_sources_diff'] = str(diff)

                tor_scripts = set(eval(row['script_domains']))
                ff_scripts = set(eval(firefox['script_domains'].values[0]))
                diff = tor_scripts.difference(ff_scripts)
                df.loc[i, 'script_domains_diff'] = str(diff)

                tor_scripts = set(eval(row['script_paths']))
                ff_scripts = set(eval(firefox['script_paths'].values[0]))
                diff = tor_scripts.difference(ff_scripts)
                df.loc[i, 'script_paths_diff'] = str(diff)

            # textual similarity (estimate of jaccard index computed using MinHash), following Niaki 2019 p7
            if not pd.isna(row.text) and not pd.isna(firefox['text'].values[0]):
                tor_textlist = row['text'].split(' ')
                tor_minhash = datasketch.MinHash()
                for d in tor_textlist:
                    tor_minhash.update(d.encode('utf8'))
                ff_textlist = firefox['text'].values[0].split(' ')
                ff_minhash = datasketch.MinHash()
                for d in ff_textlist:
                    ff_minhash.update(d.encode('utf8'))
                df.loc[i, 'text_minhash'] = tor_minhash.jaccard(ff_minhash)

                # difference in text (words that appear on Tor page but not on FF page)
                text_diff = set(tor_textlist).difference(ff_textlist)
                df.loc[i, 'text_diff'] = " ".join(list(text_diff))
    df.loc[pd.isna(df['text']), 'text'] = ''
    df.loc[pd.isna(df['text_diff']), 'text_diff'] = ''
    df.loc[pd.isna(df['script_sources']), 'script_sources'] = str(set())
    df.loc[pd.isna(df['script_domains']), 'script_domains'] = str(set())
    df.loc[pd.isna(df['script_paths']), 'script_paths'] = str(set())
    df.loc[pd.isna(df['script_sources_diff']), 'script_sources_diff'] = str(set())
    df.loc[pd.isna(df['script_domains_diff']), 'script_domains_diff'] = str(set())
    df.loc[pd.isna(df['script_paths_diff']), 'script_paths_diff'] = str(set())
    return df


def compute_khattak1(df):
    # first algorithm from Khattak (p10): 200 response code in Firefox, non-200 in Tor
    for group in tqdm(df.groupby(by=['topsite', 'subsite']), desc='Khattak (1)'):
        # group[0] is a tuple with the topsite and subsite
        # group[1] is a data frame with the data for the group
        firefox = group[1][group[1].browser == 'Firefox']
        tor = group[1][group[1].browser == 'Tor Browser']
        for i, row in tor.iterrows():
            if firefox.iloc[0].first_status == 200 and (row.first_status < 200 or row.first_status >= 300):
                df.loc[i, "khattak1"] = 'blocked'
                df.loc[i, 'khattak1_bin'] = 0
            else:
                df.loc[i, "khattak1"] = 'ok'
                df.loc[i, 'khattak1_bin'] = 1
    return df


def compute_khattak2(df):
    # second algorithm from Khattak (p12):
    # "look for cases where the Tor request is blocked and the non-Tor request is unblocked"
    # "We consider a URL ‘blocked’ if the request 1) timed out, 2) was rejected, or 3) received an HTTP response
    # with status code 400 or higher."
    #  1) and 2): we can use presence of screenshot as proxy
    for group in tqdm(df.groupby(by=['topsite', 'subsite']), desc='Khattak (2)'):
        firefox = group[1][group[1].browser == 'Firefox'].iloc[0, :]
        tor = group[1][group[1].browser == 'Tor Browser']
        ffblocked = False
        if pd.isna(firefox.screenshot) or firefox.first_status >= 400:
            ffblocked = True
        for i, row in tor.iterrows():
            torblocked = False
            if pd.isna(row.screenshot) or row.first_status >= 400:
                torblocked = True
            if torblocked and not ffblocked:
                df.loc[i, "khattak2"] = 'blocked'
                df.loc[i, 'khattak2_bin'] = 0
            else:
                df.loc[i, "khattak2"] = 'ok'
                df.loc[i, 'khattak2_bin'] = 1
    return df


def compute_singh(df, do_manual=True, hashname='phash_32_10'):
    # - algorithm from Singh
    # exclude cases where client did not receive a single HTTP response, either in Firefox or Tor
    # (eliminates client/network errors)
    # -> skip rows when pd.isna(row.first_status)
    # "no discrimination" when phash < threshold (0.40 in their case)
    # "discrimination" when phash > threshold (0.75)
    # "discrimination when Firefox has 200 status, but Tor has error status (400 or higher?)
    # manually tag remaining cases based on screenshots
    hashcol = hashname + "_diff"
    bin_name = "singh_bin_" + hashname
    max_phash_diff = df[hashcol].max()
    nodisc_threshold = 0.4 * max_phash_diff
    disc_threshold = 0.75 * max_phash_diff
    df['singh'] = None

    for group in tqdm(df.groupby(by=['topsite', 'subsite']), desc='Singh'):
        ffrow = group[1][group[1].browser == 'Firefox'].iloc[0, :]
        tor = group[1][group[1].browser == 'Tor Browser']
        for i, row in tor.iterrows():
            # skip cases where either ff or tor did not receive a response
            if pd.isna(ffrow.first_status) or pd.isna(row.first_status):
                continue
            if row[hashcol] < nodisc_threshold:
                df.loc[i, "singh"] = 'ok'
                df.loc[i, bin_name] = 1
                continue
            if row[hashcol] > disc_threshold:
                df.loc[i, "singh"] = 'blocked'
                df.loc[i, bin_name] = 0
                continue
            if ffrow.first_status == 200 and row.first_status >= 400:
                df.loc[i, "singh"] = 'blocked'
                df.loc[i, bin_name] = 0
                continue
            df.loc[i, "singh"] = 'manual'
            if do_manual:
                if not pd.isna(ffrow.screenshot) and not pd.isna(row.screenshot):
                    screenshots = [ffrow.screenshot, row.screenshot]
                    manual_check_qt(df, i, screenshots)

    df.loc[(df.singh == 'ok') | (df.singh == 'ok (m)'), 'singh_final'] = 1
    df.loc[(df.singh == 'blocked') | (df.singh == 'blocked (m)'), 'singh_final'] = 0
    return df


def plot_blocks_per_site_per_exit(data, label_col, suffix=''):
    # blocking events per site per exit node, x=exit nodes, y=sites (Khattak Fig. 5)
    filtered_data = data[(data.browser == "Tor Browser") & (~pd.isna(data[label_col]))][['topsite', 'site', 'exit', label_col]]
    filtered_data[label_col] = 1 - filtered_data[label_col]
    # filtered_data.loc[filtered_data.khattak == 'ok', 'khattak'] = 1
    # filtered_data.loc[filtered_data.khattak == 'blocked', 'khattak'] = 0
    # filtered_data.khattak = filtered_data['khattak'].astype('int')
    # make 2d data structure
    # filtered_data_p = filtered_data.pivot(index='site', columns='exit', values=label_col)
    filtered_data_p = filtered_data.pivot_table(index='topsite', columns='exit', values=label_col, aggfunc=np.mean)
    # order by number of exit nodes that were blocked
    filtered_data_p['num_ok'] = filtered_data_p.sum(1)
    # filtered_data_p['count'] = filtered_data_p.count(1)
    # filtered_data_p['ok_diff'] = filtered_data_p['count'] - filtered_data_p['num_ok']
    filtered_data_p.sort_values(by='num_ok', inplace=True, ascending=False)
    print("Highest block rate for any site:", max(filtered_data_p.num_ok))
    print("Number of sites that block more than 60% of nodes:", sum(filtered_data_p.num_ok>60))
    # remove rows that were never blocked
    # filtered_data_p = filtered_data_p[filtered_data_p.num_ok < filtered_data_p.num_ok.max()]
    del filtered_data_p['num_ok']
    # del filtered_data_p['count']
    # del filtered_data_p['ok_diff']
    # plot a heatmap. black = blocked
    plt.figure(figsize=(12, 12))
    g = sns.heatmap(filtered_data_p[:100], cmap='Blues', yticklabels=True, xticklabels=False, cbar=False)
    g.set(ylabel="", xlabel="")
    sns.despine()
    plt.savefig(f'figures/heatmap_{label_col}{suffix}.pdf', bbox_inches='tight', dpi=300)
    plt.close()


def plot_block_rate_by_exit_prob(data, label_col):
    x = label_col
    y = 'exit_prob'
    block_label = 0  # TODO: veryify block label for all classifiers
    block_rates = data.groupby('exit')[x].value_counts(normalize=True).mul(100).rename('Blocking rate').reset_index()
    block_rates = block_rates[block_rates[label_col] == block_label]
    for i, row in block_rates.iterrows():
        block_rates.loc[i, 'age'] = get_age(row.exit)
        block_rates.loc[i, 'exit_prob'] = get_exit_prob(row.exit)*100

    # Blocking rate vs exit node probability (Khattak Fig. 6)
    plt.figure(figsize=size)
    g = sns.scatterplot(data=block_rates, x='exit_prob', y='Blocking rate')
    g.set_xscale('log')
    g.xaxis.set_major_formatter(matplotlib.ticker.PercentFormatter())
    sns.despine()
    plt.savefig(f'figures/block_rate_vs_exit_prob_{label_col}.pdf', bbox_inches='tight', dpi=300)
    plt.close()

    # Blocking rate vs exit node probability vs exit node age (Khattak Fig. 4)
    plt.figure(figsize=size)
    g = sns.scatterplot(data=block_rates, x='exit_prob', y='Blocking rate', size='age')
    g.set_xscale('log')
    g.xaxis.set_major_formatter(matplotlib.ticker.PercentFormatter())
    sns.despine()
    plt.savefig(f'figures/block_rate_vs_exit_prob_vs_age_{label_col}.pdf', bbox_inches='tight', dpi=300)
    plt.close()


def plot_cdf_blocked_exits(data, label_col, suffix=''):
    x = label_col
    block_label = 0

    # CDF for fraction of exit nodes discriminated against by site (Singh Fig. 7)
    # block_rates_by_exit = data.groupby('exit')[x].value_counts(normalize=True).rename(
    #     'Fraction of exit nodes discriminated against by site').reset_index()
    # block_rates_by_exit = block_rates_by_exit[block_rates_by_exit[x] == block_label]
    plt.figure(figsize=size)
    # g = sns.ecdfplot(data=block_rates_by_exit, x='Fraction of exit nodes discriminated against by site', log_scale=False)
    g = sns.ecdfplot(data=1 - data.groupby('site')[x].mean(), log_scale=False)
    g.set(xlim=[0, 1])
    g.set(ylabel="CDF of sites", xlabel="Fraction of exit nodes discriminated against by site")
    sns.despine()
    plt.savefig(f'figures/exit_nodes_cdf_{label_col}{suffix}.pdf', bbox_inches='tight', dpi=300)
    plt.close()


def plot_cdf_blocking_sites(data, label_col, suffix=''):
    x = label_col
    block_label = 0
    plt.figure(figsize=size)
    # CDF for fraction of sites discriminating against exit node (Singh Fig. 7)
    # block_rates_by_site = data.groupby('site')[x].value_counts(normalize=True).rename(
    #     'Fraction of sites discriminating against exit node').reset_index()
    # block_rates_by_site = block_rates_by_site[block_rates_by_site[x] == block_label]
    # g = sns.ecdfplot(data=block_rates_by_site, x='Fraction of sites discriminating against exit node', log_scale=False)
    g = sns.ecdfplot(data=1-data.groupby('exit')[x].mean(), log_scale=False)
    g.set(xlim=[0, 1])
    g.set(ylabel="CDF of exit nodes", xlabel="Fraction of sites discriminating against exit node")
    sns.despine()
    plt.savefig(f'figures/sites_cdf_{label_col}{suffix}.pdf', bbox_inches='tight', dpi=300)
    plt.close()


def plot_block_rate_vs_bandwidth_age_openness(data, label_col):
    block_label = 0
    # block_rates = data.groupby('exit')[label_col].value_counts(normalize=True).mul(100).rename('Block rate').reset_index()
    block_rates = (1-data.groupby('exit')[label_col].mean()).rename('Block rate').reset_index()
    # block_rates = block_rates[block_rates[label_col] == block_label]
    for i, row in block_rates.iterrows():
        block_rates.loc[i, 'age'] = get_age(row.exit)
        block_rates.loc[i, 'exit_prob'] = get_exit_prob(row.exit)*100
        block_rates.loc[i, 'bandwidth'] = get_bandwidth(row.exit)
        block_rates.loc[i, 'ports'] = get_num_ports(row.exit)

    # Blocking rate vs exit node probability vs exit node age and bandwidth (Singh Fig. 9)
    plt.figure(figsize=size)
    g = sns.scatterplot(data=block_rates, x='bandwidth', y='Block rate', size='ports', hue='age', legend=None)
    # g.set_xscale('log')
    # g.xaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter())
    sns.despine()
    plt.savefig(f'figures/block_rate_vs_bandwidth_vs_age_vs_openness_{label_col}.pdf', bbox_inches='tight', dpi=300)
    plt.close()

    # Correlation between block rate and age/bandwidth/ports
    print("Correlation of block rates with exit node characteristics:")
    r, p = st.pearsonr(block_rates['Block rate'], block_rates.age)
    print(f"age: r={r:.3}, p={p:.3}")
    r, p = st.pearsonr(block_rates['Block rate'], block_rates.exit_prob)
    print(f"exit probability: r={r:.3}, p={p:.3}")
    r, p = st.pearsonr(block_rates['Block rate'], block_rates.bandwidth)
    print(f"bandwidth: r={r:.3}, p={p:.3}")
    r, p = st.pearsonr(block_rates['Block rate'], block_rates.ports)
    print(f"ports: r={r:.3}, p={p:.3}")


def plot_block_rate_by_hoster(data, label_col):
    x = label_col
    block_label = 0
    # block rate by hosting provider (Singh Figs. 8 and 10)

    # Cloudfront block rate vs bandwidth, openness, age (Singh Fig. 10)
    cloudfront_data = data[(~pd.isna(data.hoster)) & (data.hoster.str.contains('Cloudfront'))]
    block_rates = cloudfront_data.groupby('exit')[x].value_counts(normalize=True).mul(100).rename('Blocking rate').reset_index()
    block_rates = block_rates[block_rates[x] == block_label]
    for i, row in block_rates.iterrows():
        block_rates.loc[i, 'age'] = get_age(row.exit)
        block_rates.loc[i, 'exit_prob'] = get_exit_prob(row.exit)*100
        block_rates.loc[i, 'bandwidth'] = get_bandwidth(row.exit)
        block_rates.loc[i, 'ports'] = get_num_ports(row.exit)

    plt.figure(figsize=size)
    g = sns.scatterplot(data=block_rates, x='bandwidth', y='Blocking rate', size='ports', hue='age')
    g.set_xscale('log')
    sns.despine()
    plt.savefig(f'figures/block_rate_cloudfront_vs_bandwidth_vs_age_vs_openness_{label_col}.pdf', bbox_inches='tight', dpi=300)
    plt.close()

    # CDF for fraction of exit nodes discriminated against by site, by hoster (Akamai, Cloudfront, Cloudflare) (Singh Fig. 8)
    data_3hosts = data[~pd.isna(data.hoster)]
    data_3hosts = data_3hosts[(data_3hosts.hoster.str.contains('Cloudfront')) |
                              (data_3hosts.hoster.str.contains('Cloudflare')) |
                              (data_3hosts.hoster.str.contains('Akamai'))]
    # if a row has two host entries: split and duplicate the row
    newrows = []
    for i, row in data_3hosts[data_3hosts.hoster.str.contains(',')].iterrows():
        hosts = row.hoster.split(',')
        if len(hosts) > 1:
            data_3hosts.loc[i, 'hoster'] = hosts[0]
            rowcopy = row
            rowcopy.hoster = hosts[1]
            newrows.append(rowcopy)
    data_3hosts = data_3hosts.append(pd.DataFrame(newrows))
    block_rates_by_exit = data_3hosts.groupby(['exit', 'hoster'])[label_col].value_counts(normalize=True).rename(
        'Fraction of exit nodes discriminated against').reset_index()
    block_rates_by_exit = block_rates_by_exit[block_rates_by_exit[label_col] == block_label]
    plt.figure(figsize=size)
    g = sns.ecdfplot(data=block_rates_by_exit, x='Fraction of exit nodes discriminated against', hue='hoster',
                     log_scale=False)
    g.set(xlim=[0, 1])
    g.set(ylabel="CDF of sites")
    sns.despine()
    plt.savefig(f'figures/exit_nodes_cdf_by_hoster_{label_col}.pdf', bbox_inches='tight', dpi=300)
    plt.close()


def plot_block_rates_by_category(data, label_col):
    x = label_col
    block_label = 0
    # block rate by site category (mcafee) (Singh Fig. 11)
    block_rates_by_exit = data[~pd.isna(data.category)].groupby(['exit', 'category'])[x].value_counts(normalize=True).rename(
        'Fraction of exit nodes discriminated against').reset_index()
    block_rates_by_exit = block_rates_by_exit[block_rates_by_exit[x] == block_label]
    plt.figure(figsize=size)
    g = sns.ecdfplot(data=block_rates_by_exit, x='Fraction of exit nodes discriminated against', hue='category',
                     log_scale=False)
    g.set(xlim=[0, 1])
    g.set(ylabel="CDF of sites")
    sns.despine()
    plt.savefig(f'figures/exit_nodes_cdf_by_category_{label_col}.pdf', bbox_inches='tight', dpi=300)
    plt.close()


def plot_khattak_vs_singh(data):
    # block rate khattak vs singh algorithm (Singh Fig. 12)
    grouped_data = data[~pd.isna(data.category)].groupby(['exit'])
    block_rates_k = grouped_data['khattak'].value_counts(normalize=True). \
        rename('Fraction of exit nodes discriminated against').reset_index()
    block_rates_s = grouped_data['singh_final'].value_counts(normalize=True). \
        rename('Fraction of exit nodes discriminated against').reset_index()
    block_rates_k = block_rates_k[block_rates_k['khattak'] == 'blocked']
    block_rates_s = block_rates_s[block_rates_s['singh_final'] == 'blocked']
    plt.figure(figsize=size)
    g = sns.ecdfplot(data=block_rates_k, x='Fraction of exit nodes discriminated against', log_scale=False, label='Khattak')
    g = sns.ecdfplot(data=block_rates_s, x='Fraction of exit nodes discriminated against', log_scale=False, color='red', ax=g, label='Singh')
    plt.legend()
    sns.despine()
    plt.savefig('figures/exit_nodes_cdf_by_algorithm.pdf', bbox_inches='tight', dpi=300)
    plt.close()


def plot_phash_distance(data, suffix=''):
    # pHash distance between screenshots. x = pHash distance, y = number of samples observed, number of buckets = 30
    # (Singh Fig. 6)
    x = pd.Series(data.phash_16_8_diff, name="pHash distance between screenshots")
    plt.figure(figsize=size)
    sns.histplot(x, stat="count", bins=30)
    sns.despine()
    plt.savefig(f'figures/phash_distances_{suffix}.pdf', bbox_inches='tight', dpi=300)
    plt.close()


def plot_exit_cdf(data):
    # CDF of exit probability (Khattak Fig. 3)
    exit_cdf_data = data[['exit_prob']].dropna()
    plt.figure(figsize=size)
    g = sns.ecdfplot(data=exit_cdf_data, x='exit_prob', log_scale=False)
    # g.set(xlim=[0, 0.25])
    sns.despine()
    plt.savefig('figures/exit_prob_cdf.pdf', bbox_inches='tight', dpi=300)
    plt.close()


def plot_first_status(data, suffix=''):
    # percentage of first status codes for each site, by browser
    x = 'first_status'
    y = 'browser'
    (data
     .groupby(y)[x]
     .value_counts(normalize=True)
     .mul(100)
     .rename('percent')
     .reset_index()
     .pipe((sns.catplot, 'data'), x=x, y='percent', hue=y, kind='bar', height=height, aspect=aspect))
    sns.despine()
    plt.savefig(f'figures/first_status_codes_{suffix}.pdf', bbox_inches='tight', dpi=300)
    plt.close()


if __name__ == "__main__":
    # id = '2021-08-30_151405_ground_truth_data'
    # if os.path.exists(id + ".csv"):
    #     data = pd.read_csv(id + ".csv", index_col=0)
    # else:
    #     data = read_data(id)
    #     data = compute_features(data)
    #     data = compute_diff_features(data)
    #     data = compute_additional_features(data)
    #     data = compute_khattak1(data)
    #     data = compute_singh(data, do_manual=True)
    #     data.to_csv(id + ".csv")

    if os.path.exists('2022-01-14_data_with_labels_aug_slim.joblib'):
        # data = load('2022-01-14_data_with_labels_aug.joblib')  # full data with tag and status columns
        data = load('2022-01-14_data_with_labels_aug_slim.joblib')  # data without tag and status columns
    else:
        data_dfs = []
        for fname in glob.glob("*-2021-10-31_data_with_labels.joblib"):
            print(fname)
            df = load(fname)
            data_dfs.append(df)
        data = pd.concat(data_dfs)
        data.reset_index(inplace=True, drop=True)

        del data['text']
        del data['text_diff']
        del data['script_sources']
        del data['script_sources_diff']
        del data['script_domains']
        del data['script_domains_diff']
        del data['script_paths']
        del data['script_paths_diff']
        data = compute_khattak1(data)
        data = compute_khattak2(data)
        data = compute_singh(data, do_manual=False, hashname='phash_16_8')

        exit_nodes = set(data.exit[~pd.isna(data.exit)])
        for node in tqdm(exit_nodes, desc='Exit node properties'):
            exit_prob = get_exit_prob(node)
            data.loc[data.exit == node, 'exit_prob'] = exit_prob*100
            data.loc[data.exit == node, 'age'] = get_age(node)
            data.loc[data.exit == node, 'bandwidth'] = get_bandwidth(node)
            data.loc[data.exit == node, 'ports'] = get_num_ports(node)
            data.loc[data.exit == node, 'country'] = get_country(node)

        t = Tranco(cache=True, cache_dir='.tranco')
        tranco_list = t.list(date='2021-05-11')
        for site in set(data.topsite):
            data.loc[data.topsite == site, 'rank'] = tranco_list.rank(site)

        if os.path.exists('domains-categories.csv'):
            old_cats = pd.read_csv('domains-categories.csv', index_col=0)
            new_domains = list(set(data.topsite).difference(set(old_cats.domain)))
        else:
            new_domains = list(set(data.topsite))
            old_cats = None
        domain_cats = get_site_categories_mcafee(new_domains)
        if old_cats is not None:
            domain_cats = domain_cats.append(old_cats)
        # store results in domains-categories.csv
        domain_cats.to_csv('domains-categories.csv')

        search_domains = list(domain_cats[domain_cats.category.str.contains("Search")].domain)
        news_domains = list(domain_cats[domain_cats.category.str.contains("News")].domain)
        shopping_domains = list(domain_cats[domain_cats.category.str.contains("Shopping")].domain)
        social_domains = list(domain_cats[domain_cats.category.str.contains("Social Network")].domain)
        data.loc[data.topsite.isin(search_domains), 'category'] = "Search"
        data.loc[data.topsite.isin(news_domains), 'category'] = "News"
        data.loc[data.topsite.isin(shopping_domains), 'category'] = "Shopping"
        data.loc[data.topsite.isin(social_domains), 'category'] = "Social Networking"

        if os.path.exists('domains-hosters.csv'):
            old_domain_df = pd.read_csv('domains-hosters.csv', index_col=0)
            new_domains = list(set(data.topsite).difference(set(old_domain_df.index)))
        else:
            new_domains = list(set(data.topsite))
            old_domain_df = None
        new_domains = [d[1:] if d.startswith('.') else d for d in new_domains]
        new_domains = [d[4:] if d.startswith('www.') else d for d in new_domains]

        resp = findcdn.main(new_domains, double_in=True, threads=2)
        domain_hoster = pd.DataFrame(json.loads(resp)['domains']).T
        if old_domain_df is not None:
            domain_hoster = domain_hoster.append(old_domain_df)
        # store results in domains-hosters.csv
        domain_hoster.to_csv('domains-hosters.csv')
        for i, row in domain_hoster.iterrows():
            hosters = list(set(row.cdns_by_names.replace("'", "").split(", ")))
            data.loc[data.topsite == row.name, 'hoster'] = ",".join(hosters)

        data['site'] = data.topsite.str.cat(data.subsite, sep=" ")
        data.loc[data.subsite == 'landing', 'visit_type'] = 'landing'
        data.loc[data.subsite.str.contains('search'), 'visit_type'] = 'search'
        data.loc[(data.subsite != 'landing') & ~(data.subsite.str.contains('search')), 'visit_type'] = 'subsite'
        dump(data, '2022-01-14_data_with_labels_aug.joblib')

        cols = list(data.columns)
        newcols = []
        for col in cols:
            if not col.startswith("tag_") and not col.startswith("status_"):
                newcols.append(col)
        data = data[newcols]
        dump(data, '2022-01-14_data_with_labels_aug_slim.joblib')

    data['f1t_c1'] = data.f1_tc1

    # number/percentage of missing screenshots
    mis_screen = len(data[pd.isna(data.screenshot)])
    print("missing screenshots:", mis_screen, "(", mis_screen/len(data) * 100, "%)")
    # missing sources
    mis_source = len(data[pd.isna(data.source)])
    print("missing sources:", mis_source, "(", mis_source / len(data) * 100, "%)")
    # missing hars
    mis_har = len(data[pd.isna(data.har)])
    print("missing har files:", mis_har, "(", mis_har / len(data) * 100, "%)")

    colors = sns.color_palette("colorblind")
    sns.set_palette("colorblind")
    sns.set_style("whitegrid")
    size = (8, 3)
    height = 6
    aspect = 1.5

    plot_exit_cdf(data[data.exit_prob > 0])
    plot_phash_distance(data, '500')
    plot_first_status(data, '500')

    featuresets = ['f1', 'f2', 'f3', 'f4', 'f5', 'f6']
    classifiers = ['c1', 'c4']
    for f, c in itertools.product(featuresets, classifiers):
        classifier = f'{f}_{c}'
        plot_blocks_per_site_per_exit(data[data.subsite == 'landing'], classifier, suffix='_landing')
        plot_blocks_per_site_per_exit(data[data.visit_type == 'search'], classifier, suffix='_search')
        plot_blocks_per_site_per_exit(data[data.visit_type == 'subsite'], classifier, suffix='_subsites')
        plot_blocks_per_site_per_exit(data, classifier)
        plot_block_rate_by_exit_prob(data, classifier)
        plot_cdf_blocked_exits(data, classifier)
        plot_cdf_blocking_sites(data, classifier)
        for hoster in ['Akamai', 'Amazon', 'Cloudfront', 'Cloudflare']:
            print("Hoster: ", hoster)
            plot_block_rate_vs_bandwidth_age_openness(data[(~pd.isna(data.hoster)) &
                                                           (data.hoster.str.contains('Cloudflare'))], classifier)
        plot_block_rate_vs_bandwidth_age_openness(data, classifier)
        plot_block_rate_by_hoster(data, classifier)
        plot_block_rates_by_category(data, classifier)
        plot_block_rates_by_category(data, classifier)

        # CDFs for landing pages only vs CDFs for subsites only
        plot_cdf_blocked_exits(data[data.subsite == 'landing'], classifier, suffix='_landing')
        plot_cdf_blocking_sites(data[data.subsite == 'landing'], classifier, suffix='_landing')
        plot_cdf_blocked_exits(data[data.subsite != 'landing'], classifier, suffix='_subsites')
        plot_cdf_blocking_sites(data[data.subsite != 'landing'], classifier, suffix='_subsites')

    for heuristic in ['singh_final', 'khattak1_bin', 'khattak2_bin']:
        plot_cdf_blocked_exits(data, heuristic)
        plot_cdf_blocking_sites(data, heuristic)

    # overall block rate for binary classifier
    x = 'f1_c1'
    overall_data = data.loc[~pd.isna(data[x]), x]
    landing_data = overall_data[data.subsite == 'landing']
    search_data = overall_data[data.subsite.str.contains('search')]
    subsite_data = overall_data[(data.subsite != 'landing') & ~(data.subsite.str.contains('search'))]
    print(f"overall block rate ({x}): {1-overall_data.mean():.3}")
    print(f"block rate for landing pages and subsites: {1-overall_data[~(data.subsite.str.contains('search'))].mean()}")
    print(f"block rate for landing pages: {1-landing_data.mean():.3}")
    print(f"block rate for searches: {1 - search_data.mean():.3}")
    print(f"block rate for subsites: {1 - subsite_data.mean():.3}")
    # Kolmogorov-Smirnov test for equality of two distributions, nonparametric (i.e., no assumption of normality)
    st.ks_2samp(landing_data, search_data, alternative='two-sided')
    cohens_d(landing_data, search_data)
    st.ks_2samp(landing_data, subsite_data, alternative='two-sided')
    cohens_d(landing_data, subsite_data)
    st.ks_2samp(search_data, subsite_data, alternative='two-sided')
    cohens_d(search_data, subsite_data)
    # Kruskal-Wallis H-test tests the null hypothesis that the population median of all of the groups are equal
    st.kruskal(landing_data, subsite_data, search_data)

    # block rates for all binary classifiers
    for f in featuresets:
        x = f + "_c1"
        overall_data = data.loc[~pd.isna(data[x]), x]
        landing_data = overall_data[data.visit_type == 'landing']
        # subsite_data = overall_data[data.visit_type == 'subsite']
        print(f"overall block rate ({x}): {1 - overall_data.mean():.3}")
        print(
            f"block rate for landing pages and subsites: {1 - overall_data[~(data.subsite.str.contains('search'))].mean()}")
        print(f"block rate for landing pages: {1 - landing_data.mean():.3}")

    # block rate by hoster
    x = 'f1_c1'
    domain_hoster = pd.read_csv('domains-hosters.csv', index_col=0)
    dfs = []
    for i, row in domain_hoster.iterrows():
        hosters = list(set(row.cdns_by_names.replace("'", "").split(", ")))
        data.loc[data.topsite == row.name, 'hoster'] = hosters[0]
        for hoster in hosters[1:]:
            copydata = data[data.topsite == row.name].copy()
            copydata.hoster = hoster
            dfs.append(copydata)
    hosterdata = data.append(dfs)
    hosterdata.loc[pd.isna(data.hoster), 'hoster'] = 'n/a'

    frequent_hosters = []
    freqs = []
    for group in hosterdata[~pd.isna(hosterdata[x])].groupby('hoster'):
        if len(set(group[1].topsite)) > 10:
            frequent_hosters.append(group[0])
            freqs.append(len(set(group[1].topsite)))
    host_freq_df = pd.DataFrame()
    host_freq_df.index = frequent_hosters
    host_freq_df['freq'] = freqs

    for feature in featuresets:
        x = feature + "_c1"
        hosterdata.loc[~pd.isna(hosterdata[x]), x + '_inv'] = 1 - hosterdata.loc[~pd.isna(hosterdata[x]), x]

        sns.set_palette("Paired")
        plt.figure(figsize=(8, 3))
        g = sns.barplot(data=hosterdata[(~pd.isna(hosterdata[x])) &
                                        (hosterdata.hoster.isin(frequent_hosters))],
                        x='hoster', y=x + '_inv', order=host_freq_df.index, hue='visit_type',
                        hue_order=['landing', 'subsite', 'search'])
        for i, freq in enumerate(host_freq_df.freq):
            plt.gca().annotate(text="n=" + str(freq), xy=(i, 0.47), horizontalalignment='center')
        plt.gca().set(xlabel="Hoster", ylabel="Block rate", ylim=[0, 0.49])
        plt.xticks(rotation=45)
        g.get_legend().set_title(None)
        g.axhline(0.167, linestyle='--', linewidth=1, color='grey')
        plt.legend(loc=(0.83, 0.6))
        sns.despine()
        plt.savefig(f'figures/block_rate_by_hoster_{x}.pdf', bbox_inches='tight', dpi=300)
        plt.close()
        # Welch's t-test to test for the null hypothesis that 2 independent samples have identical average (expected) values
        print("Welch's t-test with Bonferroni for all visits")
        for hoster in frequent_hosters:
            d1 = hosterdata[(~pd.isna(hosterdata[x])) & (hosterdata.hoster == hoster)][x]
            d2 = hosterdata[(~pd.isna(hosterdata[x])) & (hosterdata.hoster != hoster)][x]
            p = st.ttest_ind(d1, d2, equal_var=False).pvalue
            d = cohens_d(d1, d2)
            print(hoster, f"p={p * len(frequent_hosters):.2}, d={d:.2}, avg={1-np.mean(d1):.2}")  # with Bonferroni correction for multiple testing
        print("Welch's t-test with Bonferroni for searches")
        for hoster in frequent_hosters:
            d1 = hosterdata[(~pd.isna(hosterdata[x])) & (hosterdata.hoster == hoster) & (hosterdata.visit_type == 'search')][x]
            d2 = hosterdata[(~pd.isna(hosterdata[x])) & (hosterdata.hoster != hoster) & (hosterdata.visit_type == 'search')][x]
            p = st.ttest_ind(d1, d2, equal_var=False).pvalue
            d = cohens_d(d1, d2)
            print(hoster, f"p={p * len(frequent_hosters):.2}, d={d:.2}, avg={1-np.mean(d1):.2}")  # with Bonferroni correction for multiple testing

    # block rate by country of exit node
    x = 'f1_c1'
    country_data = 1-data.groupby('country')[x].mean()
    sns.barplot(data=data[~pd.isna(data[x])], x='country', y=x, order=country_data.sort_values().index)
    p = st.ttest_ind(data[(~pd.isna(data[x])) & (data.country == 'az')][x],
                     data[(~pd.isna(data[x])) & (data.country == 'nz')][x], equal_var=False).pvalue
    d = cohens_d(data[(~pd.isna(data[x])) & (data.country == 'az')][x],
                     data[(~pd.isna(data[x])) & (data.country == 'nz')][x])
    print(f"block rates for nz vs az: p={p:.2}, d={d:.2}")

    # block rates by other characteristics of exit nodes (all not significant)
    sns.lineplot(data=data[~pd.isna(data[x])], x='bandwidth', y=x)
    sns.lineplot(data=data[~pd.isna(data[x])], x='age', y=x)
    sns.lineplot(data=data[~pd.isna(data[x])], x='ports', y=x)
    sns.lineplot(data=data[~pd.isna(data[x])], x='exit_prob', y=x)

    # block rates by site category
    domains_cats = pd.read_csv('domains-categories.csv', index_col=0)
    dfs = []
    for i, row in domains_cats.iterrows():
        copydata = data[data.topsite == row.domain].copy()
        copydata['category'] = row.category
        dfs.append(copydata)
    catdata = pd.concat(dfs)

    frequent_cats = []
    freqs = []
    for group in catdata[~pd.isna(catdata[x])].groupby('category'):
        if len(set(group[1].topsite)) > 14:
            frequent_cats.append(group[0])
            freqs.append(len(set(group[1].topsite)))
    freq_df = pd.DataFrame()
    freq_df.index = frequent_cats
    freq_df['freq'] = freqs

    for feature in featuresets:
        x = feature + "_c1"
        catdata.loc[~pd.isna(catdata[x]), x + '_inv'] = 1 - catdata.loc[~pd.isna(catdata[x]), x]

        sns.set_palette("Paired")
        plt.figure(figsize=(13*.85, 4*.7))
        g = sns.barplot(data=catdata[(~pd.isna(catdata[x])) & (catdata.category.isin(frequent_cats))],
                        x='category', y=x + '_inv', hue='visit_type', order=freq_df.index)
        for i, freq in enumerate(freq_df.freq):
            plt.gca().annotate(text="n=" + str(freq), xy=(i, 0.67), horizontalalignment='center', size=9.5)
        plt.gca().set(xlabel="Category", ylabel="Block rate", ylim=[0, 0.69])
        plt.xticks(rotation=45, ma='right', ha='right')
        g.get_legend().set_title(None)
        plt.legend(loc=(0.9, 0.6))
        g.axhline(0.167, linestyle='--', linewidth=1, color='grey')
        sns.despine()
        plt.savefig(f'figures/block_rate_by_category_{x}.pdf', bbox_inches='tight', dpi=300)
        plt.close()
        print("block rate for searches on search engines that are not Google:",
              1-catdata[(~pd.isna(catdata[x])) & (catdata.category == 'Search Engines') & (catdata.visit_type == 'search') &
                        ~(catdata.topsite.str.contains("google"))][x].mean())
        print("block rate for searches on Google search engine:",
              1-catdata[(~pd.isna(catdata[x])) & (catdata.category == 'Search Engines') & (catdata.visit_type == 'search') &
                        (catdata.topsite.str.contains("google"))][x].mean())
        # Welch's t-test to test for the null hypothesis that 2 independent samples have identical average (expected) values
        for cat in frequent_cats:
            d1 = catdata[(~pd.isna(catdata[x])) & (catdata.category == cat) & (catdata.visit_type.isin(['landing', 'subsite']))][x]
            d2 = catdata[(~pd.isna(catdata[x])) & (catdata.category != cat) & (catdata.visit_type.isin(['landing', 'subsite']))][x]
            p = st.ttest_ind(d1, d2, equal_var=False).pvalue
            d = cohens_d(d1, d2)
            print(cat, f"p={p*len(frequent_cats):.2}, d={d:.2}, avg={1-np.mean(d1):.2}")  # with Bonferroni correction for multiple testing

        print("Welch's t-test for whether landing+subsite have different block rates")
        for cat in frequent_cats:
            d1 = catdata[(~pd.isna(catdata[x])) & (catdata.category == cat) & (catdata.visit_type.isin(['landing']))][x]
            d2 = catdata[(~pd.isna(catdata[x])) & (catdata.category == cat) & (catdata.visit_type.isin(['subsite']))][x]
            p = st.ttest_ind(d1, d2, equal_var=False).pvalue
            d = cohens_d(d1, d2)
            print(cat, f"p={p*len(frequent_cats):.2}, d={d:.2}, avg landing={1-np.mean(d1):.2},"
                       f"avg subsite={1-np.mean(d2):.2}")  # with Bonferroni correction for multiple testing

    # block rate by rank
    data['binned_rank'] = 10000
    data.loc[data['rank'] < 10000, 'binned_rank'] = 1000
    data.loc[data['rank'] < 1000, 'binned_rank'] = 100
    data.loc[data['rank'] < 100, 'binned_rank'] = 1

    block_rates = (1 - data.groupby(['rank', 'visit_type'])[x].mean()).rename('Block rate').reset_index()
    block_rates = block_rates[~pd.isna(block_rates['Block rate'])]
    r, p = st.pearsonr(block_rates[block_rates.visit_type == 'landing']['Block rate'],
                block_rates[block_rates.visit_type == 'landing']['rank'])
    print(f"block rate/rank correlation for landing pages: r={r:.2}, p={p:.2}")
    r, p = st.pearsonr(block_rates[block_rates.visit_type == 'subsite']['Block rate'],
                block_rates[block_rates.visit_type == 'subsite']['rank'])
    print(f"block rate/rank correlation for subsites: r={r:.2}, p={p:.2}")
    r, p = st.pearsonr(block_rates[block_rates.visit_type == 'search']['Block rate'],
                block_rates[block_rates.visit_type == 'search']['rank'])
    print(f"block rate/rank correlation for search pages: r={r:.2}, p={p:.2}")

    # block percentages for Singh and Khattak algorithms
    y = 'browser'
    for x in ['f1_c1', 'singh', 'khattak1', 'khattak2']:
        (data
         .groupby(y)[x]
         .value_counts(normalize=True)
         .mul(100)
         .rename('percent')
         .reset_index()
         .pipe((sns.catplot, 'data'), x=x, y='percent', hue=y, kind='bar', height=height, aspect=aspect))
        sns.despine()
        plt.savefig(f'figures/block_percentages_{x}.pdf', bbox_inches='tight', dpi=300)
        plt.close()

    # count sites per exit node to find exit notes that have stopped working
    exit_numbers = data[~pd.isna(data.har)].groupby('exit')['exit'].value_counts()
    exit_numbers.plot(kind='bar')
    print(f"exit nodes with low number of collected sites (average = {np.mean(exit_numbers)}):\n",
          exit_numbers[exit_numbers < 0.9*np.mean(exit_numbers)])

    # agreement of heuristics with our manual training data labels
    data_name1 = '2021-08-30_151405_ground_truth_data'
    data_name2 = '2021-09-12_123705_ground_truth_data2'
    data_name3 = '2021-09-13_120141_ground_truth_data3'
    data_name4 = '2021-10-10_023311_ground_truth_data4'
    data_names = [data_name1, data_name2, data_name3, data_name4]
    training_data = pd.DataFrame()
    for name in data_names:
        if os.path.exists(name + "-labeled.csv"):
            temp = pd.read_csv(name + "-labeled.csv", index_col=0)
            training_data = training_data.append(temp)
    training_data = compute_khattak1(training_data)
    training_data = compute_khattak2(training_data)
    training_data = compute_singh(training_data, do_manual=False)
    training_data = compute_singh(training_data, hashname='whash_32', do_manual=False)
    training_data = training_data[~pd.isna(training_data.lookalike)]
    training_data['site'] = training_data.topsite.str.cat(training_data.subsite, sep=" ")
    training_data.loc[training_data.subsite == 'landing', 'visit_type'] = 'landing'
    training_data.loc[training_data.subsite.str.contains('search'), 'visit_type'] = 'search'
    training_data.loc[(training_data.subsite != 'landing') & ~(training_data.subsite.str.contains('search')), 'visit_type'] = 'subsite'

    print("Stats for *training* data collection:")
    print("number of visited topsites:", len(set(training_data[training_data.visit_type == 'landing'].topsite)))
    print("number of visited subsites:", len(set(training_data[training_data.visit_type == 'subsite'].site)))
    print("number of executed searches:", len(set(training_data[training_data.visit_type == 'search'].site)),
          "(on", len(set(training_data[training_data.visit_type == 'search'].topsite)), "different topsites)")
    print("number of Google domains:", len(set(training_data[training_data.topsite.str.contains("google")])),
          "(",
          len(set(training_data[training_data.topsite.str.contains("google")])) / len(set(training_data[training_data.visit_type == 'landing'].topsite)),
          "%)")

    # rank distribution in training data
    t = Tranco(cache=True, cache_dir='.tranco')
    tranco_list = t.list(date='2021-05-11')
    ranks = []
    for url in set(training_data.topsite):
        ranks.append(tranco_list.rank(url))
    ranks = sorted(ranks)
    plt.figure(figsize=(5, 2.2))
    g = sns.histplot(ranks, log_scale=True, bins=30)
    g.set(ylabel="Number of sites", xlabel="Tranco rank")
    sns.despine()
    plt.savefig('figures/training-data-ranks.pdf', bbox_inches='tight', dpi=300)
    plt.close()
    print("Number of topsites in training data:", len(set(training_data.topsite)))
    print("Label distribution in training data:\n", training_data.lookalike.value_counts())

    # binary label for block (0) vs same (1)
    training_data['bin_label1'] = np.nan
    training_data.loc[(training_data.lookalike == 'block'), 'bin_label1'] = 0
    training_data.loc[training_data.lookalike == 'same', 'bin_label1'] = 1

    # binary label for block+captcha (0) vs same (1) - remaining samples should be unlabeled/omitted
    training_data['bin_label2'] = np.nan
    training_data.loc[(training_data.lookalike == 'block') | (training_data.lookalike == 'captcha'), 'bin_label2'] = 0
    training_data.loc[training_data.lookalike == 'same', 'bin_label2'] = 1

    # agreement heuristics with manual labels (block vs same)
    print("## Agreement of heuristics with manual labels (binary: blocked=block, ok=same)")
    ag_data1 = training_data[~pd.isna(training_data.bin_label1)]
    print("Khattak1", metrics.classification_report(ag_data1.bin_label1, ag_data1.khattak1_bin, digits=3))
    print("Khattak2", metrics.classification_report(ag_data1.bin_label1, ag_data1.khattak2_bin, digits=3))
    ag_data1_fil = ag_data1[~pd.isna(ag_data1.singh_bin_phash_32_10)]
    print("Singh", metrics.classification_report(ag_data1_fil.bin_label1, ag_data1_fil.singh_bin_phash_32_10, digits=3))

    # agreement heuristics with manual labels (block+captcha vs same)
    print("## Agreement of heuristics with manual labels (binary: blocked=block+captcha, ok=same)")
    ag_data2 = training_data[~pd.isna(training_data.bin_label2)]
    print(metrics.classification_report(ag_data2.bin_label2, ag_data2.khattak1_bin, digits=3))
    print(metrics.classification_report(ag_data2.bin_label2, ag_data2.khattak2_bin, digits=3))
    ag_data2_fil = ag_data2[~pd.isna(ag_data2.singh_bin_phash_32_10)]
    print("Singh", metrics.classification_report(ag_data2_fil.bin_label2, ag_data2_fil.singh_bin_phash_32_10, digits=3))

    # captchas
    x = 'f5_c4'
    # new column to compute captcha rate
    xc = x + '_captcha'
    data.loc[(~pd.isna(data[x])) & (data[x] == 1), xc] = 1
    data.loc[(~pd.isna(data[x])) & (data[x] != 1), xc] = 0
    overall_data = data.loc[~pd.isna(data[x]), xc]
    landing_data = overall_data[data.subsite == 'landing']
    search_data = overall_data[data.subsite.str.contains('search')]
    subsite_data = overall_data[(data.subsite != 'landing') & ~(data.subsite.str.contains('search'))]
    print(f"overall captcha rate ({x}): {overall_data.mean():.3}")

    print(f"captcha rate for landing pages and subsites: {overall_data[~(data.subsite.str.contains('search'))].mean():.3}")
    print(f"captcha rate for landing pages: {landing_data.mean():.3}")
    print(f"captcha rate for searches: {search_data.mean():.3}")
    print(f"captcha rate for subsites: {subsite_data.mean():.3}")
    # Kolmogorov-Smirnov test for equality of two distributions, nonparametric (i.e., no assumption of normality)
    st.ks_2samp(landing_data, search_data, alternative='two-sided')
    cohens_d(landing_data, search_data)
    st.ks_2samp(landing_data, subsite_data, alternative='two-sided')
    cohens_d(landing_data, subsite_data)
    st.ks_2samp(search_data, subsite_data, alternative='two-sided')
    cohens_d(search_data, subsite_data)
    # Kruskal-Wallis H-test tests the null hypothesis that the population median of all of the groups are equal
    st.kruskal(landing_data, subsite_data, search_data)

    catdata.loc[(~pd.isna(catdata[x])) & (catdata[x] == 1), xc] = 1
    catdata.loc[(~pd.isna(catdata[x])) & (catdata[x] != 1), xc] = 0

    sns.set_palette("Paired")
    plt.figure(figsize=(13*.85, 4*.7))
    g = sns.barplot(data=catdata[(~pd.isna(catdata[xc])) & (catdata.category.isin(frequent_cats))],
                    x='category', y=xc, hue='visit_type', order=freq_df.index)
    for i, freq in enumerate(freq_df.freq):
        plt.gca().annotate(text="n=" + str(freq), xy=(i, 0.22), horizontalalignment='center', size=9.5)
    plt.gca().set(xlabel="Category", ylabel="Captcha rate", ylim=[0, 0.23])
    plt.xticks(rotation=45, ma='right', ha='right')
    g.get_legend().set_title(None)
    plt.legend(loc=(0.9, 0.6))
    g.axhline(0.00443, linestyle='--', linewidth=1, color='grey')  # overall captcha rate
    sns.despine()
    plt.savefig(f'figures/captcha_rate_by_category_{xc}.pdf', bbox_inches='tight', dpi=300)
    plt.close()

    # Welch's t-test to test for the null hypothesis that 2 independent samples have identical average (expected) values
    for cat in frequent_cats:
        d1 = catdata[(~pd.isna(catdata[xc])) & (catdata.category == cat) &
                     (catdata.visit_type.isin(['landing', 'subsite', 'search']))][xc]
        d2 = catdata[(~pd.isna(catdata[xc])) & (catdata.category != cat) &
                     (catdata.visit_type.isin(['landing', 'subsite', 'search']))][xc]
        p = st.ttest_ind(d1, d2, equal_var=False).pvalue
        d = cohens_d(d1, d2)
        print(cat, f"p={p * len(frequent_cats):.2}, d={d:.2}, avg={np.mean(d1):.2}")  # with Bonferroni correction for multiple testing

    hosterdata.loc[(~pd.isna(hosterdata[x])) & (hosterdata[x] == 1), xc] = 1
    hosterdata.loc[(~pd.isna(hosterdata[x])) & (hosterdata[x] != 1), xc] = 0

    sns.set_palette("Paired")
    plt.figure(figsize=(8, 3))
    g = sns.barplot(data=hosterdata[(~pd.isna(hosterdata[xc])) &
                                    (hosterdata.hoster.isin(frequent_hosters))],
                    x='hoster', y=xc, order=host_freq_df.index, hue='visit_type',
                    hue_order=['landing', 'subsite', 'search'])
    for i, freq in enumerate(host_freq_df.freq):
        plt.gca().annotate(text="n=" + str(freq), xy=(i, 0.11), horizontalalignment='center')
    plt.gca().set(xlabel="Hoster", ylabel="Captcha rate", ylim=[0, 0.12])
    plt.xticks(rotation=45)
    g.get_legend().set_title(None)
    g.axhline(0.00443, linestyle='--', linewidth=1, color='grey')  # overall captcha rate
    plt.legend(loc=(0.02, 0.59))
    sns.despine()
    plt.savefig(f'figures/captcha_rate_by_hoster_{xc}.pdf', bbox_inches='tight', dpi=300)
    plt.close()
    # Welch's t-test to test for the null hypothesis that 2 independent samples have identical average (expected) values
    for hoster in frequent_hosters:
        d1 = hosterdata[(~pd.isna(hosterdata[xc])) & (hosterdata.hoster == hoster)][xc]
        d2 = hosterdata[(~pd.isna(hosterdata[xc])) & (hosterdata.hoster != hoster)][xc]
        p = st.ttest_ind(d1, d2, equal_var=False).pvalue
        d = cohens_d(d1, d2)
        print(hoster, f"p={p  * len(frequent_hosters):.2}, d={d:.2}")

    plot_block_rate_vs_bandwidth_age_openness(data, xc)
    country_data = data.groupby('country')[xc].mean().sort_values()
    p = st.ttest_ind(data[(~pd.isna(data[xc])) & (data.country == 'it')][xc],
                     data[(~pd.isna(data[xc])) & (data.country == 'nl')][xc], equal_var=False).pvalue
    d = cohens_d(data[(~pd.isna(data[xc])) & (data.country == 'it')][xc],
                 data[(~pd.isna(data[xc])) & (data.country == 'nl')][xc])
    print(f"average captcha rate for it: {data[(~pd.isna(data[xc])) & (data.country == 'it')][xc].mean():.2}")
    print(f"average captcha rate for nl: {data[(~pd.isna(data[xc])) & (data.country == 'nl')][xc].mean():.2}")
    print(f"captcha rates for it vs nl: p={p:.2}, d={d:.2}")

    block_rates = (data.groupby(['rank', 'visit_type'])[xc].mean()).rename('Captcha rate').reset_index()
    block_rates = block_rates[~pd.isna(block_rates['Captcha rate'])]
    r, p = st.pearsonr(block_rates[block_rates.visit_type == 'landing']['Captcha rate'],
                block_rates[block_rates.visit_type == 'landing']['rank'])
    print(f"Captcha rate/rank correlation for landing pages: r={r:.2}, p={p:.2}")
    r, p = st.pearsonr(block_rates[block_rates.visit_type == 'subsite']['Captcha rate'],
                block_rates[block_rates.visit_type == 'subsite']['rank'])
    print(f"Captcha rate/rank correlation for subsites: r={r:.2}, p={p:.2}")
    r, p = st.pearsonr(block_rates[block_rates.visit_type == 'search']['Captcha rate'],
                block_rates[block_rates.visit_type == 'search']['rank'])
    print(f"Captcha rate/rank correlation for search pages: r={r:.2}, p={p:.2}")

    print("Stats for data collection:")
    print("number of visited topsites:", len(set(data[data.visit_type == 'landing'].topsite)))
    print("number of visited subsites:", len(set(data[data.visit_type == 'subsite'].site)))
    print("number of executed searches:", len(set(data[data.visit_type == 'search'].site)),
          "(on", len(set(data[data.visit_type == 'search'].topsite)), "different topsites)")
    print("number of Google domains:", len(set(data[data.topsite.str.contains("google")])),
          "(",
          len(set(data[data.topsite.str.contains("google")])) / len(set(data[data.visit_type == 'landing'].topsite)),
          "%)")
