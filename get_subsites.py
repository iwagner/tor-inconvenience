import time

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.common.exceptions import TimeoutException, UnexpectedAlertPresentException, WebDriverException
from selenium.webdriver.firefox.options import Options
from selenium.webdriver import Firefox, FirefoxProfile
from selenium.webdriver.support.wait import WebDriverWait
from tranco import Tranco
import os
import random
import pandas as pd
from pathlib import Path
import argparse
import json
import hashlib


def extract_links(response, uri, req='https'):
    page = BeautifulSoup(response, features="lxml")
    links = page.find_all('a')
    crawl_links = []
    for link in links:
        add_link = True
        href = link.get('href', None)

        if href is None or len(href) == 0 or href.startswith('#') or href.startswith('javascript'):
            # href is empty or '#'
            add_link = False
            continue

        if href.startswith('//'):
            href = req + ':' + href

        if not href.startswith('http'):
            href = req + '://' + uri + href

        # make sure that only links from this topsite are included.
        if not uri in href:
            add_link = False
            continue

        if add_link:
            crawl_links.append(href)

    return list(set(crawl_links))


def get_subsites(topsites, csvname, num_subsites):
    with open("settings.json", 'r') as f:
        paths = json.load(f)

    options = Options()
    options.headless = True

    subs_list = []
    for i, uri in enumerate(topsites):
        driver = Firefox(firefox_binary=(paths["Firefox"] + "/firefox-bin"), executable_path=paths["GeckoDriver"],
                         options=options)
        driver.install_addon(Path(__file__).resolve().parent.as_posix() + '/cookies.xpi')
        driver.set_page_load_timeout(30)
        print(i, "/", len(topsites))

        try:
            # retrieve index page via https
            driver.get(f"https://{uri}/")
        except Exception as inst:
            print(inst)
        else:
            response = driver.page_source
            crawl_links = extract_links(response, uri, 'https')

        try:
            # retrieve index page via http
            driver.get(f"http://{uri}/")
        except Exception as inst:
            print(inst)
        else:
            response = driver.page_source
            crawl_links.extend(extract_links(response, uri, 'http'))

            # select args.s random links from the index page
            sample_size = num_subsites
            if len(crawl_links) < sample_size:
                sample_size = len(crawl_links)
            links = random.sample(crawl_links, k=sample_size)
            for link in links:
                subs_list.append([uri, link, hashlib.sha1(link.encode()).hexdigest()])
            subs_list.append([uri, uri, 'landing'])
            # links.insert(0, uri)
            # subsites = subsites.append(pd.DataFrame([links]))

        driver.quit()

    subsites = pd.DataFrame(subs_list, columns=['topsite', 'subsite', 'hash'])
    subsites.to_csv(csvname)


def get_subsites_from_idx(num_topsites, num_subsites, first=0):
    """
    Fetches subsite URLs for the specified number of topsites (top-n from Tranco list) and saves to a csv.
    :param num_topsites: Number of topsites to fetch subsite URLs for.
    :param num_subsites: Number of subsite URLs to fetch for each topsite.
    :param first: First index in tranco list to fetch.
    :return Pandas DataFrame with one row per topsite and columns for the subsites.
    """
    csvname = f"subsites-{num_topsites}-{num_subsites}-{first}.csv"

    if not os.path.exists(csvname):
        # file for the number of topsites and subsites does not exist yet

        t = Tranco(cache=True, cache_dir='.tranco')
        date_list = t.list(date='2021-05-11')
        # topsites = date_list.top(num_topsites)
        topsites = date_list.top(num_topsites + first)[first:]
        get_subsites(topsites, csvname, num_subsites)

    # load the file and return.
    subsites = pd.read_csv(csvname, index_col=0)
    return subsites


def get_subsites_from_csv(csv, num_subsites):
    """
    Fetches subsite URLs for the topsites specified in the given CSV file and saves to a csv.
    :param csv: CSV file with one topsite per line
    :param num_subsites: Number of subsite URLs to fetch for each topsite.
    :return: Pandas DataFrame with one row per topsite and columns for the subsites.
    """
    csvname = f"subsites-{csv}-{num_subsites}.csv"
    if not os.path.exists(csvname):
        topsites = list(pd.read_csv(csv, header=None)[0].values)
        get_subsites(topsites, csvname, num_subsites)

    # load the file and return.
    subsites = pd.read_csv(csvname, index_col=0)
    return subsites


def get_subsites_from_hispar(num_subsites, first_subsite_idx=0):
    csvname = '2021-10-20-hispar-subsites.csv'
    subsites = pd.read_csv(csvname, index_col=0)
    if num_subsites > 0:
        sel_subsites = pd.DataFrame()
        groups = subsites.groupby('topsite').groups
        for topsite in groups.keys():
            sel_subsites = sel_subsites.append(subsites.iloc[groups[topsite][first_subsite_idx:first_subsite_idx+num_subsites]])
        return sel_subsites
    else:
        return subsites


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-n", help="number of top sites to use", metavar="100", default="2", type=int)
    parser.add_argument("-s", help="number of subsites to retrieve", metavar="5", default="3", type=int)
    args = parser.parse_args()

    root_dir = Path(__file__).resolve().parent

    subsites = get_subsites_from_idx(args.n, args.s)
