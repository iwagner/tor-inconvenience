import os
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier, ExtraTreesClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_selection import SelectFromModel
from scipy.sparse import hstack, csr_matrix
from enum import Enum
import pickle
from tqdm import tqdm
import tarfile
from pathlib import Path
import shutil
from joblib import dump, load
import analyze


class BlockLabel(Enum):
    BLOCK = 0
    CAPTCHA = 1
    MISSING = 2
    SLOW = 3
    COOKIE = 4
    NOCOOKIE = 5
    OTHER = 6
    SAME = 7


def nopunct(text):
    return text.replace('/', '').replace(':', '').replace('.', '').replace('-', '').replace('=', '').replace('_', '')


def compare(df, algo, hashname=''):
    colname = algo + "_" + 'bin'
    if hashname is not '':
        colname = colname + "_" + hashname

    # remove rows with na entries because singh_bin has nan entries where the singh algorithm would require
    # manual checking of screenshots.
    df = df[~pd.isna(df[colname])]

    print("### 0 means block+captcha+missing elements")
    print(algo + " " + hashname + " confusion matrix\n",
          metrics.confusion_matrix(df.bin_label, df[colname]))
    print(metrics.classification_report(df.bin_label, df[colname]))

    df = df[~pd.isna(df['bin_label2'])]
    print("### 0 means block+captcha")
    print(algo + " " + hashname + " confusion matrix\n",
          metrics.confusion_matrix(df.bin_label2, df[colname]))
    print(metrics.classification_report(df.bin_label2, df[colname]))


class TorClassifiers:
    def __init__(self, ground_truth_filename, evaluate=False):
        self.hash_names = ['phash_8_4', 'phash_16_8', 'phash_32_10', 'phash_32_16', 'whash_8', 'whash_16', 'whash_32']
        self.df_cols = ['bin_label', 'bin_label2', 'browser', 'exit', 'har', 'index', 'label',
                        'level_0', 'lookalike', 'phash_16_8', 'phash_32_10', 'phash_32_16',
                        'phash_8_4', 'relpath', 'screenshot', 'script_domains',
                        'script_domains_diff', 'script_paths', 'script_paths_diff',
                        'script_sources', 'script_sources_diff', 'source', 'subsite', 'text',
                        'text_diff', 'topsite', 'whash_16', 'whash_32', 'whash_8']
        self.all_name = ground_truth_filename
        self.f1_df_3 = None
        self.f1_df_4 = None
        self.f1_3 = None
        self.f1_4 = None
        self.f2_df_3 = None
        self.f2_df_4 = None
        self.f2_3 = None
        self.f2_4 = None
        self.f3_df_3 = None
        self.f3_df_4 = None
        self.f3_3 = None
        self.f3_4 = None
        self.f4_df_3 = None
        self.f4_df_4 = None
        self.f4_3 = None
        self.f4_4 = None
        self.f5_df_3 = None
        self.f5_df_4 = None
        self.f5_3 = None
        self.f5_4 = None
        self.f6_df_3 = None
        self.f6_df_4 = None
        self.f6_3 = None
        self.f6_4 = None
        self.feature_pickle_name = '2021-10-13_feature_elimination_dict.pkl'
        if os.path.exists(self.feature_pickle_name):
            self.feature_elim = pickle.load(open(os.path.join(self.feature_pickle_name), mode="rb"))
        else:
            self.feature_elim = dict()

        if evaluate:
            self.import_eval_data()
        else:
            self.import_training_data()

    def compare_with_heuristics(self, data):
        df = analyze.compute_khattak1(data)
        df = analyze.compute_khattak2(df)
        for hash in self.hash_names:
            df = analyze.compute_singh(df, do_manual=False, hashname=hash)

        # compare my manual labels with singh/khattak heuristics
        compare(df, 'khattak1')
        compare(df, 'khattak2')
        for hash in self.hash_names:
            compare(df, 'singh', hashname=hash)

    def import_training_data(self):
        if os.path.exists(self.all_name):
            data = pd.read_csv(self.all_name, index_col=0)
        else:
            data_name1 = '2021-08-30_151405_ground_truth_data'
            data_name2 = '2021-09-12_123705_ground_truth_data2'
            data_name3 = '2021-09-13_120141_ground_truth_data3'
            data_name4 = '2021-10-10_023311_ground_truth_data4'
            data_names = [data_name1, data_name2, data_name3, data_name4]
            data = pd.DataFrame()
            for name in data_names:
                if os.path.exists(name + "-labeled.csv"):
                    temp = pd.read_csv(name + "-labeled.csv", index_col=0)
                    data = data.append(temp)
                else:
                    print(f"Labeled data file {name} not found, please run label.py first.")
                    exit(0)
            # if os.path.exists(data_name2 + '-labeled.csv'):
            #     data = data.append(pd.read_csv(data_name2 + "-labeled.csv", index_col=0))
            # else:
            #     print(f"Labeled data file {data_name2} not found, please run label.py first.")
            #     exit(0)
            # if os.path.exists(data_name3 + '-labeled.csv'):
            #     data = data.append(pd.read_csv(data_name3 + "-labeled.csv", index_col=0))
            # else:
            #     print(f"Labeled data file {data_name3} not found, please run label.py first.")
            #     exit(0)

            cols_to_retain = ['browser', 'topsite', 'subsite', 'exit', 'screenshot', 'source', 'har', 'relpath',
                              'lookalike']
            data = data[cols_to_retain]
            data = data.reset_index()
            data = analyze.compute_features(data)
            data = analyze.compute_diff_features(data)
            data.to_csv(self.all_name)

        # select only rows from data that have been labeled (missing labels due to, e.g., missing screenshots)
        data = data[~pd.isna(data.lookalike)]

        # update default value for text_diff column to empty string instead of nan
        data.loc[pd.isna(data['text_diff']), 'text_diff'] = ''

        # turn lookalike column into enum labels
        data['label'] = np.nan
        data.loc[data.lookalike == 'block', 'label'] = BlockLabel.BLOCK
        data.loc[data.lookalike == 'captcha', 'label'] = BlockLabel.CAPTCHA
        data.loc[data.lookalike == 'missing', 'label'] = BlockLabel.MISSING
        data.loc[data.lookalike == 'slow', 'label'] = BlockLabel.SLOW
        data.loc[data.lookalike == 'cookie', 'label'] = BlockLabel.COOKIE
        data.loc[data.lookalike == 'nocookie', 'label'] = BlockLabel.NOCOOKIE
        data.loc[data.lookalike == 'other', 'label'] = BlockLabel.OTHER
        data.loc[data.lookalike == 'same', 'label'] = BlockLabel.SAME

        # binary label for inconvenience (0) vs no inconvenience (1)
        data['bin_label'] = 1
        data.loc[(data.lookalike == 'block') | (data.lookalike == 'captcha') | (data.lookalike == 'missing'), 'bin_label'] = 0

        # binary label for block+captcha (0) vs same (1) - remaining samples should be unlabeled/omitted
        data['bin_label2'] = np.nan
        data.loc[(data.lookalike == 'block') | (data.lookalike == 'captcha'), 'bin_label2'] = 0
        data.loc[data.lookalike == 'same', 'bin_label2'] = 1

        # remove rows with nan values
        data = data[~pd.isna(data.perc2xx_diff)]
        data = data[~pd.isna(data.unif_diff)]
        data = data[(data.lookalike == 'block') | (data.lookalike == 'captcha') | (data.lookalike == 'missing')
                    | (data.lookalike == 'same')]
        data_4features = data[~pd.isna(data.text)]
        data_3features = data_4features[~pd.isna(data_4features.bin_label2)]

        self.f1_df_4, self.f1_4 = self.append_text_features(data_4features, diff=True, html_only=False)
        self.f1_df_3, self.f1_3 = self.append_text_features(data_3features, diff=True, html_only=False)
        self.f2_df_4, self.f2_4 = self.append_text_features(data_4features, diff=False, html_only=False)
        self.f2_df_3, self.f2_3 = self.append_text_features(data_3features, diff=False, html_only=False)
        self.f3_df_4, self.f3_4 = self.append_text_features(data_4features, diff=True, html_only=True)
        self.f3_df_3, self.f3_3 = self.append_text_features(data_3features, diff=True, html_only=True)
        self.f4_df_4, self.f4_4 = self.append_text_features(data_4features, diff=True, html_only=False, http_only=True)
        self.f4_df_3, self.f4_3 = self.append_text_features(data_3features, diff=True, html_only=False, http_only=True)
        self.f5_df_4, self.f5_4 = self.append_text_features(data_4features, diff=False, html_only=True, http_only=False)
        self.f5_df_3, self.f5_3 = self.append_text_features(data_3features, diff=False, html_only=True, http_only=False)
        self.f6_df_4, self.f6_4 = self.append_text_features(data_4features, diff=False, html_only=False, http_only=True)
        self.f6_df_3, self.f6_3 = self.append_text_features(data_3features, diff=False, html_only=False, http_only=True)

    def import_eval_data(self):
        data_dir = Path(__file__).parent.resolve() / 'dockerdata'  # as_posix()
        temp_dir = Path(__file__).parent.resolve() / '.temp'

        dfname = '2021-10-31_data.pkl'
        df_diffname = '2021-10-31_data_with_diff.pkl'
        data_dfs = []

        if os.path.exists(dfname):
            # existing_df = pd.read_csv(dfname, index_col=0)
            existing_df = pickle.load(open(dfname, 'rb'))
            data_dfs.append(existing_df)
            existing_files = set(existing_df['tgzname'])
        else:
            existing_df = pd.DataFrame()
            existing_files = []

        tgz_files = os.listdir(data_dir)
        for tgz in tqdm(tgz_files, desc='import'):
            if tgz in existing_files:
                # skip, we have already imported this one
                continue
            temp_dir.mkdir(exist_ok=True)
            tar = tarfile.open(name=os.path.join(data_dir, tgz))
            tar.extractall(temp_dir)

            data = analyze.read_data(temp_dir.as_posix())
            data = analyze.compute_features(data)
            data['tgzname'] = tgz
            data_dfs.append(data)

            shutil.rmtree(temp_dir)

        df = pd.concat(data_dfs)
        df.reset_index(inplace=True, drop=True)
        if len(df) > len(existing_df):
            pickle.dump(df, open(dfname, 'wb'))

        if os.path.exists(df_diffname):
            diff_df = pickle.load(open(df_diffname, 'rb'))
            df = diff_df
        else:
            diff_df = pd.DataFrame()
        # if len(diff_df) < len(df):
        #     # only compute the diff features again if we imported new data
        #     df = analyze.compute_diff_features(df)
        #     pickle.dump(df, open(df_diffname, 'wb'))
        # else:
        #     df = diff_df
        df.loc[pd.isna(df['text']), 'text'] = ''
        df.loc[pd.isna(df['script_sources']), 'script_sources'] = str(set())
        df.loc[pd.isna(df['script_domains']), 'script_domains'] = str(set())
        df.loc[pd.isna(df['script_paths']), 'script_paths'] = str(set())
        self.f1_df_4, self.f1_4 = self.append_text_features(df, diff=True, html_only=False, evaluate=True)

    def append_text_features(self, data, diff=True, html_only=False, http_only=False, evaluate=False,
                             vectorizer_basename='vectorizers/2021-11-19_vectorizer-'):
        vecname = ''
        if diff:
            vecname += 'diff-'
        if html_only:
            vecname += 'htmlonly-'
        if http_only:
            vecname += 'httponly-'

        if evaluate:
            vectorizer = pickle.load(open(vectorizer_basename + vecname + 'text.pkl', 'rb'))
            x_text = vectorizer.transform(list(data.text))
            vectorizer = pickle.load(open(vectorizer_basename + vecname + 'text-diff.pkl', 'rb'))
            x_text_diff = vectorizer.transform(list(data.text_diff))
        else:
            vectorizer = TfidfVectorizer(stop_words='english', ngram_range=(1, 2))
            x_text = vectorizer.fit_transform(list(data.text))
            pickle.dump(vectorizer, open(vectorizer_basename + vecname + 'text.pkl', 'wb'))
            x_text_diff = vectorizer.fit_transform(list(data.text_diff))
            pickle.dump(vectorizer, open(vectorizer_basename + vecname + 'text-diff.pkl', 'wb'))

        c_source_diff = [nopunct(" ".join(eval(sd))) for sd in data.script_sources_diff]
        c_path_diff = [nopunct(" ".join(eval(sd))) for sd in data.script_paths_diff]
        c_domain_diff = [nopunct(" ".join(eval(sd))) for sd in data.script_domains_diff]

        c_scriptsource = [" ".join(eval(ss)) for ss in data.script_sources]
        c_scriptdomains = [" ".join(eval(ss)) for ss in data.script_domains]
        c_scriptpaths = [" ".join(eval(ss)) for ss in data.script_paths]

        if evaluate:
            vectorizer2 = pickle.load(open(vectorizer_basename + vecname + 'source-diff.pkl', 'rb'))
            x_source_diff = vectorizer2.transform(c_source_diff)
            vectorizer2 = pickle.load(open(vectorizer_basename + vecname + 'path-diff.pkl', 'rb'))
            x_path_diff = vectorizer2.transform(c_path_diff)
            vectorizer2 = pickle.load(open(vectorizer_basename + vecname + 'domain-diff.pkl', 'rb'))
            x_domain_diff = vectorizer2.transform(c_domain_diff)

            vectorizer2 = pickle.load(open(vectorizer_basename + vecname + 'source.pkl', 'rb'))
            x_scriptsource = vectorizer2.transform(c_scriptsource)
            vectorizer2 = pickle.load(open(vectorizer_basename + vecname + 'domain.pkl', 'rb'))
            x_scriptdomain = vectorizer2.transform(c_scriptdomains)
            vectorizer2 = pickle.load(open(vectorizer_basename + vecname + 'path.pkl', 'rb'))
            x_scriptpath = vectorizer2.transform(c_scriptpaths)
        else:
            vectorizer2 = CountVectorizer()
            x_source_diff = vectorizer2.fit_transform(c_source_diff)
            pickle.dump(vectorizer2, open(vectorizer_basename + vecname + 'source-diff.pkl', 'wb'))
            x_path_diff = vectorizer2.fit_transform(c_path_diff)
            pickle.dump(vectorizer2, open(vectorizer_basename + vecname + 'path-diff.pkl', 'wb'))
            x_domain_diff = vectorizer2.fit_transform(c_domain_diff)
            pickle.dump(vectorizer2, open(vectorizer_basename + vecname + 'domain-diff.pkl', 'wb'))

            x_scriptsource = vectorizer2.fit_transform(c_scriptsource)
            pickle.dump(vectorizer2, open(vectorizer_basename + vecname + 'source.pkl', 'wb'))
            x_scriptdomain = vectorizer2.fit_transform(c_scriptdomains)
            pickle.dump(vectorizer2, open(vectorizer_basename + vecname + 'domain.pkl', 'wb'))
            x_scriptpath = vectorizer2.fit_transform(c_scriptpaths)
            pickle.dump(vectorizer2, open(vectorizer_basename + vecname + 'path.pkl', 'wb'))

        # append columns for script-related features extracted by CountVectorizer
        if diff:
            script_names = ['source_diff', 'path_diff', 'domain_diff', 'scriptsource', 'scriptdomain', 'scriptpath']
        else:
            script_names = ['scriptsource', 'scriptdomain', 'scriptpath']
        df = data.reset_index()

        for name in script_names:
            var = eval("x_" + name)
            colnames = [name + "_" + str(i) for i in range(var.shape[1])]
            df = pd.concat([df, pd.DataFrame(var.toarray(), columns=colnames)], axis=1)

        cols = self.get_feature_columns(df, diff, html_only, http_only)
        extcols = self.df_cols + cols

        # make a sparse matrix that has the columns from the dense dataframe and all tf-idf features:
        newcols = csr_matrix(df[cols])
        if diff:
            all_features = hstack([x_text_diff, x_text, newcols])
        else:
            all_features = hstack([x_text, newcols])

        if http_only:
            return df[extcols], newcols
        else:
            return df[extcols], all_features

    def get_feature_columns(self, data, diff=True, html_only=False, http_only=False):
        cols = []
        # based on screenshots
        # screenshot hashes by themselves don't make much sense?
        # but, more importantly, sparse matrix representation and sklearn can't deal with object data types
        # cols.extend(self.hash_names)

        if not html_only:
            # based on HTTP requests/responses
            cols.extend(['first_status', 'num_2xx', 'num_3xx', 'num_5xx'])
            cols.extend(['num_requests', 'num_cookie_reqs', 'num_cookie_resps', 'num_responses'])

        # based on HTML source
        if not http_only:
            cols.extend(['source_size', 'num_tags'])

        if diff:
            # based on screenshots
            if not http_only:
                cols.extend([h + '_diff' for h in self.hash_names])

            if not html_only:
                # based on HTTP requests/responses
                cols.extend(['perc2xx_diff', 'perc3xx_diff', 'perc5xx_diff'])
                cols.extend(['num_requests_diff', 'num_cookie_reqs_diff', 'num_cookie_resps_diff', 'numresp_diff'])
                cols.extend([c for c in data.columns if c.startswith('status_')])

            if not http_only:
                # based on HTML source
                cols.extend(['source_diff', 'source_perc_diff', 'unif_diff', 'num_tags_diff', 'text_minhash'])
                cols.extend([c for c in data.columns if c.startswith('tag_')])
                script_names = ['source_diff', 'path_diff', 'domain_diff', 'scriptsource', 'scriptdomain', 'scriptpath']

        else:
            if not html_only:
                # based on HTTP requests/responses
                cols.extend([c for c in data.columns if c.startswith('status_') and not c.endswith('diff')])

            if not http_only:
                # based on HTML source
                cols.extend([c for c in data.columns if c.startswith('tag_') and not c.endswith('diff')])
                script_names = ['scriptsource', 'scriptdomain', 'scriptpath']

        if not http_only:
            # based on HTML source
            for name in script_names:
                cols.extend([c for c in data.columns if c.startswith(name)])
        return cols

    def get_data(self, featureset, num_labels):
        if featureset == 'f1':
            if num_labels == 3:
                return self.f1_df_3, self.f1_3
            else:
                return self.f1_df_4, self.f1_4
        elif featureset == 'f2':
            if num_labels == 3:
                return self.f2_df_3, self.f2_3
            else:
                return self.f2_df_4, self.f2_4
        elif featureset == 'f3':
            if num_labels == 3:
                return self.f3_df_3, self.f3_3
            else:
                return self.f3_df_4, self.f3_4
        elif featureset == 'f4':
            if num_labels == 3:
                return self.f4_df_3, self.f4_3
            else:
                return self.f4_df_4, self.f4_4
        elif featureset == 'f5':
            if num_labels == 3:
                return self.f5_df_3, self.f5_3
            else:
                return self.f5_df_4, self.f5_4
        elif featureset == 'f6':
            if num_labels == 3:
                return self.f6_df_3, self.f6_3
            else:
                return self.f6_df_4, self.f6_4

        if featureset in self.feature_elim.keys():
            cols = self.feature_elim[featureset]['cols']
            mask = self.feature_elim[featureset]['mask']
        else:
            cols = None
            mask = None

        if 'f1' in featureset:
            features = 'f1'
        elif 'f2' in featureset:
            features = 'f2'
        elif 'f3' in featureset:
            features = 'f3'
        elif 'f4' in featureset:
            features = 'f4'
        elif 'f5' in featureset:
            features = 'f5'
        elif 'f6' in featureset:
            features = 'f6'
        else:
            raise ValueError(f"Featureset {featureset} not supported.")

        if 't' in featureset:
            func = self.get_features_tree_elim
        elif 'r' in featureset:
            func = self.get_features_rec_elim
        else:
            raise ValueError(f"Feature elimination function in {featureset} not supported.")

        if 'c1' in featureset:
            classifier = self.train_c1
            labels = 3
        elif 'c2' in featureset:
            classifier = self.train_c2
            labels = 4
        elif 'c3' in featureset:
            classifier = self.train_c3
            labels = 4
        elif 'c4' in featureset:
            classifier = self.train_c4
            labels = 3
        else:
            raise ValueError(f"Classifier in {featureset} not supported.")

        df = eval(f'self.{features}_df_{labels}')
        csr = eval(f'self.{features}_{labels}')

        if cols is None or mask is None:
            # perform the feature elimination
            cols, mask = func(features, labels, classifier)
        # return filtered dataset
        return df[self.df_cols + cols], csr.tocsr()[:, mask]

    def get_features_tree_elim(self, featureset, num_labels, clf_trainer):
        # Tree-based feature selection
        # https://scikit-learn.org/stable/modules/feature_selection.html
        data, matrix = self.get_data(featureset, num_labels)
        clf = clf_trainer(featureset, print_eval=False, random_state=0, n_estimators=100)
        diff = True
        html_only = False
        http_only = False
        if "f2" in featureset:
            diff = False
        if "f3" in featureset:
            html_only = True
        if "f4" in featureset:
            http_only = True
        if "f5" in featureset:
            html_only = True
            diff = False
        if "f6" in featureset:
            http_only = True
            diff = False
        cols = self.get_feature_columns(data, diff=diff, html_only=html_only, http_only=http_only)
        # this is very quick.
        model = SelectFromModel(clf, prefit=True)
        # X_new = model.transform(matrix)
        mask = model.get_support(indices=False)
        # below only selects the named columns that are in data, but omits the additional columns from matrix
        sel_cols_tree = [val for is_sel, val in zip(model.get_support(), cols) if is_sel]
        featurename = featureset + 't' + clf_trainer.__name__[-2:]
        if featurename not in self.feature_elim.keys():
            self.feature_elim[featurename] = dict()
        self.feature_elim[featurename]['cols'] = sel_cols_tree
        self.feature_elim[featurename]['mask'] = mask
        pickle.dump(self.feature_elim, open(self.feature_pickle_name, mode="wb"))
        return sel_cols_tree, mask

    def get_features_rec_elim(self, featureset, num_labels, clf_trainer):
        # Recursive feature elimination with cross-validation
        # https://scikit-learn.org/stable/auto_examples/feature_selection/plot_rfe_with_cross_validation.html#sphx-glr-auto-examples-feature-selection-plot-rfe-with-cross-validation-py
        from sklearn.model_selection import StratifiedKFold
        from sklearn.feature_selection import RFECV
        import matplotlib.pyplot as plt
        min_features_to_select = 10  # Minimum number of features to consider
        data, matrix = self.get_data(featureset, num_labels)
        diff = True
        html_only = False
        http_only = False
        if "f2" in featureset:
            diff = False
        if "f3" in featureset:
            html_only = True
        if "f4" in featureset:
            http_only = True
        if "f5" in featureset:
            html_only = True
            diff = False
        if "f6" in featureset:
            http_only = True
            diff = False
        cols = self.get_feature_columns(data, diff=diff, html_only=html_only, http_only=http_only)
        clf = clf_trainer(featureset, print_eval=False, random_state=0, n_estimators=100)
        rfecv = RFECV(estimator=clf, step=0.005, cv=StratifiedKFold(5),
                      scoring='f1_macro',
                      min_features_to_select=min_features_to_select,
                      n_jobs=12)

        # y should depend on the selected classifier type
        if clf_trainer.__name__.endswith("c1"):
            y = np.array(data.bin_label2)
        elif clf_trainer.__name__.endswith("c2"):
            y = np.array(data.bin_label)
        elif clf_trainer.__name__.endswith("c3") or clf_trainer.__name__.endswith("c4"):
            y = [label.value for label in np.array(data.label)]
        # This takes ~hours when running with 20k features.
        # With step=500 and k=2, this takes ~1 hour to eliminate from ~470k features.
        # With step=0.005 (i.e., half a percent of features eliminated at each step)
        # and k=5, this takes ~30 minutes to eliminate from ~470k features. This is also better at reducing the number
        # of features (step=500 results in 401 features for f1rc1, compared to 101 features for step=0.005), with same
        # classifier performance.
        rfecv.fit(matrix, y)

        print("Optimal number of features : %d" % rfecv.n_features_)
        mask = rfecv.get_support(indices=False)
        sel_cols_rec = [val for is_good, val in zip(rfecv.support_, cols) if is_good]

        # Plot number of features VS. cross-validation scores
        plt.figure()
        plt.xlabel("Number of features selected")
        plt.ylabel("Cross validation score (accuracy)")
        plt.plot(range(min_features_to_select,
                       len(rfecv.grid_scores_) + min_features_to_select),
                 rfecv.grid_scores_)
        plt.show()
        featurename = featureset + 'r' + clf_trainer.__name__[-2:]
        if featurename not in self.feature_elim.keys():
            self.feature_elim[featurename] = dict()
        self.feature_elim[featurename]['cols'] = sel_cols_rec
        self.feature_elim[featurename]['mask'] = mask
        pickle.dump(self.feature_elim, open(self.feature_pickle_name, mode="wb"))
        return sel_cols_rec, mask

    def train_c1(self, featureset='f1', classifier=RandomForestClassifier, print_eval=True, **kwargs):
        # use 3-label dataset and bin_label2 column
        data, matrix = self.get_data(featureset, num_labels=3)
        x = matrix
        y_bin2 = np.array(data.bin_label2)
        train_inputs2, val_inputs2, train_binlabels2, val_binlabels2 = train_test_split(x, y_bin2, test_size=0.3,
                                                                                        random_state=42,
                                                                                        stratify=y_bin2)
        clf_binary2 = classifier(**kwargs)  # for RF: random_state=0, n_estimators=100, max_features='sqrt'
        clf_binary2.fit(train_inputs2, train_binlabels2)
        if print_eval:
            val_binpredicted2 = clf_binary2.predict(val_inputs2)
            print("### Binary (block+captcha)", classifier.__name__, "\n",
                  metrics.classification_report(val_binlabels2, val_binpredicted2, digits=3, zero_division=0))
            report = metrics.classification_report(val_binlabels2, val_binpredicted2, digits=3, zero_division=0,
                                                   output_dict=True)
            report_df = pd.DataFrame(report).transpose()
            print(report_df.to_latex(float_format="%.3f"))
        return clf_binary2

    def train_c2(self, featureset='f1', classifier=RandomForestClassifier, print_eval=True, **kwargs):
        # use 4-label dataset and bin_label column
        data, matrix = self.get_data(featureset, num_labels=4)
        x = matrix
        y_bin = np.array(data.bin_label)
        train_inputs, val_inputs, train_binlabels, val_binlabels = train_test_split(x, y_bin, test_size=0.3,
                                                                                    random_state=42, stratify=y_bin)
        clf_binary = classifier(**kwargs)
        clf_binary.fit(train_inputs, train_binlabels)
        if print_eval:
            val_binpredicted = clf_binary.predict(val_inputs)
            print("### Binary (block+captcha+missing)", classifier.__name__, "\n",
                  metrics.classification_report(val_binlabels, val_binpredicted, digits=3, zero_division=0))
            report = metrics.classification_report(val_binlabels, val_binpredicted, digits=3, zero_division=0,
                                                   output_dict=True)
            report_df = pd.DataFrame(report).transpose()
            print(report_df.to_latex(float_format="%.3f"))
        return clf_binary

    def train_c3(self, featureset='f1', classifier=RandomForestClassifier, print_eval=True, **kwargs):
        # use 4-label dataset and label column
        data, matrix = self.get_data(featureset, num_labels=4)
        x = matrix
        y = [label.value for label in np.array(data.label)]
        train_inputs, val_inputs, train_labels, val_labels = train_test_split(x, y, test_size=0.3, random_state=42,
                                                                              stratify=y)
        clf_multilabel = classifier(**kwargs)
        clf_multilabel.fit(train_inputs, train_labels)
        if print_eval:
            val_predicted = clf_multilabel.predict(val_inputs)
            levels = [BlockLabel(0).name, BlockLabel(1).name, BlockLabel(2).name, BlockLabel(7).name]
            print("### Multi-label", classifier.__name__, "\n",
                  metrics.classification_report(val_labels, val_predicted, digits=3, zero_division=0,
                                                labels=list(set(y)), target_names=levels))
            report = metrics.classification_report(val_labels, val_predicted, digits=3, zero_division=0,
                                                   labels=list(set(y)), target_names=levels, output_dict=True)
            report_df = pd.DataFrame(report).transpose()
            print(report_df.to_latex(float_format="%.3f"))

        return clf_multilabel

    def train_c4(self, featureset='f1', classifier=RandomForestClassifier, print_eval=True, **kwargs):
        # use 3-label dataset and label column
        data, matrix = self.get_data(featureset, num_labels=3)
        x = matrix
        y = [label.value for label in np.array(data.label)]
        train_inputs, val_inputs, train_labels, val_labels = train_test_split(x, y, test_size=0.3, random_state=42,
                                                                              stratify=y)
        clf_multilabel = classifier(**kwargs)
        clf_multilabel.fit(train_inputs, train_labels)
        if print_eval:
            val_predicted = clf_multilabel.predict(val_inputs)
            levels = [BlockLabel(0).name, BlockLabel(1).name, BlockLabel(7).name]
            print("### Multi-label", classifier.__name__, "\n",
                  metrics.classification_report(val_labels, val_predicted, digits=3, zero_division=0,
                                                labels=list(set(y)), target_names=levels))
            report = metrics.classification_report(val_labels, val_predicted, digits=3, zero_division=0,
                                                   labels=list(set(y)), target_names=levels, output_dict=True)
            report_df = pd.DataFrame(report).transpose()
            print(report_df.to_latex(float_format="%.3f"))

        return clf_multilabel

    def train_best(self):
        # train and persist the best classifiers with algorithms and parameter settings determined by hyperparameter
        # tuning
        f1c1_clf = self.train_c1(featureset='f1rc1', classifier=RandomForestClassifier, criterion='gini',
                                 max_depth=None, max_features='sqrt', n_estimators=100, random_state=42)
        dump(f1c1_clf, 'models/f1c1_clf.joblib')

        f1tc1_clf = self.train_c1(featureset='f1tc1', classifier=ExtraTreesClassifier, criterion='gini',
                                  max_depth=None, max_features='sqrt', n_estimators=100, random_state=42)
        dump(f1tc1_clf, 'models/f1tc1_clf.joblib')

        f1ac1_clf = self.train_c1(featureset='f1', classifier=GradientBoostingClassifier, learning_rate=0.5,
                                  max_depth=None, max_features='sqrt', n_estimators=100, random_state=42)
        dump(f1ac1_clf, 'models/f1ac1_clf.joblib')

        f1c4_clf = self.train_c4(featureset='f1tc4', classifier=AdaBoostClassifier, learning_rate=0.5,
                                 n_estimators=1000)
        dump(f1c4_clf, 'models/f1c4_clf.joblib')

        f2c1_clf = self.train_c1(featureset='f2tc1', classifier=DecisionTreeClassifier, criterion='entropy',
                                 max_depth=None, max_features='sqrt', splitter='best', random_state=42)
        dump(f2c1_clf, 'models/f2c1_clf.joblib')

        f2c4_clf = self.train_c4(featureset='f2tc4', classifier=DecisionTreeClassifier, criterion='entropy',
                                 max_depth=None, max_features='sqrt', splitter='best', random_state=42)
        dump(f2c4_clf, 'models/f2c4_clf.joblib')

        f3c1_clf = self.train_c1(featureset='f3rc1', classifier=RandomForestClassifier, criterion='gini',
                                 max_depth=None, max_features='sqrt', n_estimators=100, random_state=42)
        dump(f3c1_clf, 'models/f3c1_clf.joblib')

        f3c4_clf = self.train_c4(featureset='f3rc4', classifier=AdaBoostClassifier, learning_rate=0.5,
                                 n_estimators=1000)
        dump(f3c4_clf, 'models/f3c4_clf.joblib')

        f4c1_clf = self.train_c1(featureset='f4', classifier=AdaBoostClassifier, learning_rate=0.1, n_estimators=500)
        dump(f4c1_clf, 'models/f4c1_clf.joblib')

        f4c4_clf = self.train_c4(featureset='f4', classifier=GradientBoostingClassifier, learning_rate=1,
                                 max_depth=None, max_features='sqrt', n_estimators=500, random_state=42)
        dump(f4c4_clf, 'models/f4c4_clf.joblib')

        f5c1_clf = self.train_c1(featureset='f5rc1', classifier=GradientBoostingClassifier, learning_rate=0.5,
                                 max_depth=5, max_features='sqrt', n_estimators=100, random_state=42)
        dump(f5c1_clf, 'models/f5c1_clf.joblib')

        f5c4_clf = self.train_c4(featureset='f5', classifier=AdaBoostClassifier, learning_rate=1, n_estimators=1000)
        dump(f5c4_clf, 'models/f5c4_clf.joblib')

        f6c1_clf = self.train_c1(featureset='f6tc1', classifier=ExtraTreesClassifier, criterion='gini',
                                 max_depth=None, max_features='sqrt', n_estimators=500, random_state=42)
        dump(f6c1_clf, 'models/f6c1_clf.joblib')

        f6c4_clf = self.train_c4(featureset='f6', classifier=GradientBoostingClassifier, learning_rate=0.01,
                                 max_depth=5, max_features='sqrt', n_estimators=1000, random_state=42)
        dump(f6c4_clf, 'models/f6c4_clf.joblib')
        