# A Machine Learning Approach to Detect Differential Treatment of Anonymous Users

This repository holds code and data for the following paper:

I. Wagner, ‘A Machine Learning Approach to Detect Differential Treatment of Anonymous Users’, European Symposium on Research in Computer Security (ESORICS), Copenhagen, Denmark, Sep. 2022. (conditionally accepted as of April 2022)


## Installation

* install Python dependencies (see requirements.txt)
* run setup.py to download Firefox, Tor Browser, geckodriver, browser extensions, and create settings.json
* extract data.zip
* Optional: create Docker container, e.g., with `docker build -t tor-collect .` (see Dockerfile for example command to run data collection)

## Usage

* main.py: crawler to collect data. Can be configured flexibly with commandline arguments to collect data based on Tranco or Hispar toplists, using specific Tor exit nodes, and with/without collecting subsites.
* analyze.py: analyze collected data and create plots, functions to compute features (run to recreate figures from the paper)
* label.py: GUI for labeling collected data
* classify.py: Hyperparameter tuning for classifiers
* tor_classifiers.py: configuration of classifiers including import of training data, definition of features for 6 feature sets, feature elimination
* import_data.py: code to import and merge data collected on local Boinc grid computing infrastructure
* get_subsites.py: used from main.py to retrieve subsites to collect data from

## Contact

* Isabel Wagner: iw _AT_ ieee _DOT_ org
