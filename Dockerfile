# to build and test container locally:
# docker build -t tor-collect .
# docker run --shm-size=2g --mount type=bind,source="$(pwd)"/dockerdata,target=/app/data tor-collect python3 main.py

FROM python:3.7-stretch

WORKDIR /app

# install required python libraries
COPY requirements.txt /app
RUN python -m pip install --upgrade pip && \
	pip install -r requirements.txt

RUN apt-get update -qqy \
  && apt-get -qqy --no-install-recommends install xvfb xserver-xephyr python3-xlib firefox-esr tor

COPY .tranco /app/.tranco/

# copy Firefox, Tor browser, geckodriver, browserup proxy
COPY firefox /app/firefox/
COPY tor-browser_en-US /app/tor-browser_en-US/
COPY geckodriver /app/

# copy settings file
COPY settings-docker.json /app/settings.json

# copy browser extensions
COPY *.xpi /app/

# copy exit node lists and hispar list
COPY 2021-10-26_exit_set_* /app/
COPY 2021-10-20-hispar-subsites.csv /app/
COPY 2021-10-20-hispar-topsites.csv /app/
COPY 2021-11-13_exit_set_* /app/

RUN Xvfb :99 -ac &
RUN export DISPLAY=:99

# copy source files
COPY *.py /app/

# define environment variable to indicate we're in a Docker container
ENV AM_I_IN_A_DOCKER_CONTAINER True
