from pathlib import Path
import json
import wget
import tarfile

install_dir = Path(__file__).resolve().parent


def main():
    print("Running setup")

    if check_file_exists("settings.json"):
        print("WARNING! A settings.json file already exists, are you sure you want to overwrite the current settings? "
              "[y/N] ", end="")
        user_input = input().upper()
        if (user_input == "Y") or (user_input == "YES"):
            print("Overwriting settings.json")
            set_paths()
        else:
            print("Skipping setup")
            return
    else:
        set_paths()

    print("Downloading browser extensions")
    wget.download("https://addons.mozilla.org/firefox/downloads/file/3802980/i_dont_care_about_cookies-3.3.1-an+fx.xpi",
                  out="cookies.xpi")

    wget.download("https://addons.mozilla.org/firefox/downloads/file/964750/har_export_trigger-0.6.1-an+fx.xpi",
                  out="har_export_trigger-0.6.1-an+fx.xpi")

    print("Setup completed successfully, settings saved to settings.json")


def set_paths():
    absolute_paths = {
        "Firefox": "",
        "Tor Browser": "",
        "GeckoDriver": ""
    }

    download_info = {
        "Firefox": {
            "url": "https://download-installer.cdn.mozilla.net/pub/firefox/releases/83.0/linux-x86_64/en-GB/firefox-83.0.tar.bz2",
            "relative_path": "firefox/"
        },
        "Tor Browser": {
            "url": "https://archive.torproject.org/tor-package-archive/torbrowser/10.0.18/tor-browser-linux64-10.0.18_en-US.tar.xz",
            "relative_path": "tor-browser_en-US/"
        },
        "GeckoDriver": {
            "url": "https://github.com/mozilla/geckodriver/releases/download/v0.29.1/geckodriver-v0.29.1-linux64.tar.gz",
            "relative_path": "geckodriver"
        }
    }

    for software in absolute_paths:
        print(f"Enter the path of {software} ({download_info[software]['relative_path']}) "
              f"[Leave blank to automatically download]: ", end="")
        software_path = input()
        if software_path != "":
            # Cleanly resolves user input to ensure consistency.
            absolute_paths[software] = str(Path(software_path).resolve())
        else:
            absolute_paths[software] = download_and_extract(software, download_info[software]["url"],
                                                            download_info[software]["relative_path"])

    with open(install_dir / "settings.json", 'w') as f:
        json.dump(absolute_paths, f, indent=2)


def check_file_exists(filename):
    if Path(install_dir / filename).is_file():
        return True
    else:
        return False


def download_and_extract(software, download_url, relative_path):
    # Returns the absolute path of the downloaded software

    # Download
    print(f"Downloading {software}")
    download_filename = wget.download(download_url)
    print()

    # Extract
    absolute_path = str((install_dir / relative_path).resolve())
    print(f"Extracting to {absolute_path}")
    tar = tarfile.open(download_filename)
    tar.extractall(install_dir)
    tar.close()

    # Delete archive after extracting
    Path(download_filename).resolve().unlink()

    print(f"Successfully downloaded {software}.")
    return absolute_path


if __name__ == "__main__":
    main()
