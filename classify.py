import numpy as np
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier, ExtraTreesClassifier
from sklearn.svm import LinearSVC, SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn import metrics
import itertools
import collections
from tor_classifiers import TorClassifiers


def tune_params(grid_clf):
    grid_clf.fit(train_inputs, train_labels)
    y_true, y_pred = val_labels, grid_clf.predict(val_inputs)
    output = ["Best parameters set found on development set:", grid_clf.best_params_, "Grid scores on development set:"]
    output.append("**Macro F1 score: %0.3f**"
                  % metrics.f1_score(y_true, y_pred, labels=list(set(y)), zero_division=0, average='macro'))
    means = grid_clf.cv_results_['mean_test_score']
    stds = grid_clf.cv_results_['std_test_score']
    for mean, std, params in zip(means, stds, grid_clf.cv_results_['params']):
        output.append("%0.3f (+/-%0.03f) for %r"
              % (mean, std * 2, params))

    output.append("Detailed classification report:")
    output.append(metrics.classification_report(y_true, y_pred, digits=3, labels=list(set(y)), zero_division=0,
                                                target_names=levels))

    return output, grid_clf.cv_results_


if __name__ == "__main__":
    all_name = '2021-10-13_all_ground_truth_labeled.csv'
    tc = TorClassifiers(all_name)

    # print number of features for each featureset/classifier combination
    featuresets = ['f1', 'f1r', 'f1t', 'f2', 'f2r', 'f2t', 'f3', 'f3r', 'f3t', 'f4', 'f4r', 'f4t',
                   'f5', 'f5r', 'f5t', 'f6', 'f6r', 'f6t']
    classifiers = ['c1', 'c2', 'c3', 'c4']
    for c, f in itertools.product(classifiers, featuresets):
        if len(f) > 2:
            dataname = f + c
        else:
            dataname = f
        if c == 'c1' or c == 'c4':
            num_labels = 3
        else:
            num_labels = 4
        data, x = tc.get_data(dataname, num_labels)
        print(f"* {f} {c}: {x.shape[1]}")

    # Hyperparameter tuning
    featuresets = [ 'f1', 'f2', 'f3', 'f4', 'f5', 'f6',
                    'f1r', 'f2r', 'f3r', 'f4r', 'f5r', 'f6r',
                    'f1t', 'f2t', 'f3t', 'f4t', 'f5t', 'f6t']
    classifiers = ['c1', 'c2', 'c3', 'c4']
    test_size = 0.3
    rand = 42
    scorer = 'f1_macro'
    algorithms = [DecisionTreeClassifier, ExtraTreesClassifier, SVC, LinearSVC, RandomForestClassifier,
                  AdaBoostClassifier, GradientBoostingClassifier]
    tuning_params = dict()
    tuning_params[DecisionTreeClassifier] = [
        {'criterion': ['gini', 'entropy'], 'splitter': ['best', 'random'], 'max_depth': [None, 5, 10],
         'max_features': ['sqrt', 'log2']}
    ]
    tuning_params[SVC] = [
        {'kernel': ['rbf', 'sigmoid'], 'gamma': ['scale', 'auto']},
        {'kernel': ['poly'], 'degree': [3], 'gamma': ['scale']}
        # poly with degree > 3 or with gamma=auto is very slow
    ]
    tuning_params[LinearSVC] = [
        {'penalty': ['l1'], 'dual': [False], 'loss': ['squared_hinge'], 'max_iter': [1000, 5000]},
        {'penalty': ['l2'], 'dual': [True], 'loss': ['hinge', 'squared_hinge'], 'max_iter': [1000, 5000]},
        {'penalty': ['l2'], 'dual': [False], 'loss': ['squared_hinge'], 'max_iter': [1000, 5000]}
    ]
    tuning_params[RandomForestClassifier] = [
        {'n_estimators': [100, 500, 1000], 'max_depth': [None, 5, 10], 'criterion': ['gini', 'entropy'],
         'max_features': ['sqrt', 'log2']}
    ]
    tuning_params[GradientBoostingClassifier] = [
        {'n_estimators': [100, 500, 1000], 'max_depth': [None, 3, 5, 10], 'learning_rate': [0.001, 0.01, 0.1, 0.5, 1],
         'max_features': ['sqrt']}
    ]
    tuning_params[AdaBoostClassifier] = [
        {'n_estimators': [50, 100, 500, 1000], 'learning_rate': [0.001, 0.01, 0.1, 0.5, 1]}
    ]
    tuning_params[ExtraTreesClassifier] = [
        {'n_estimators': [100, 500, 1000], 'max_depth': [None, 5, 10], 'criterion': ['gini', 'entropy'],
         'max_features': ['sqrt', 'log2']}
    ]
    algo_params = dict()
    algo_params[DecisionTreeClassifier] = {'random_state': rand}
    algo_params[SVC] = {'random_state': rand}
    algo_params[LinearSVC] = {'random_state': rand}
    algo_params[RandomForestClassifier] = {'random_state': rand, 'bootstrap': True, 'oob_score': True}
    algo_params[ExtraTreesClassifier] = {'random_state': rand, 'bootstrap': True, 'oob_score': True}
    algo_params[GradientBoostingClassifier] = {'random_state': rand}
    algo_params[AdaBoostClassifier] = {}

    nested_dict = lambda: collections.defaultdict(nested_dict)
    outputs = nested_dict()

    for f, c in itertools.product(featuresets, classifiers):
        print("### ", f, c)
        if len(f) > 2:
            dataname = f + c
        else:
            dataname = f
        if c == 'c1' or c == 'c4':
            num_labels = 3
        else:
            num_labels = 4
        data, x = tc.get_data(dataname, num_labels)
        if c == 'c1':
            y = np.array(data.bin_label2)
            levels = ['block', 'same']
        elif c == 'c2':
            y = np.array(data.bin_label)
            levels = ['block/captcha', 'same']
        elif c == 'c3':
            # works for both c3 and c4
            y = [label.value for label in np.array(data.label)]
            levels = ['block', 'captcha', 'missing', 'same']
        else:
            y = [label.value for label in np.array(data.label)]
            levels = ['block', 'captcha', 'same']
        train_inputs, val_inputs, train_labels, val_labels = train_test_split(x, y, test_size=test_size,
                                                                              random_state=rand, stratify=y)
        for alg in algorithms:
            print("## ", alg)
            clf = GridSearchCV(alg(**algo_params[alg]),
                               tuning_params[alg], scoring=scorer, return_train_score=True, verbose=2, n_jobs=12, cv=10)
            output, results = tune_params(clf)
            with open('output-hyperparameter-tuning.txt', 'a') as outfile:
                outfile.write(f"## {f} {c} {alg.__name__}\n")
                for line in output:
                    outfile.write(str(line) + '\n')

            output.insert(0, "## " + str(alg))
            outputs[alg][f][c] = output

    # train and persist best classifiers with best parameter settings
    tc.train_best()
