import glob
import os
import pickle
import shutil
import tarfile
from pathlib import Path

import datasketch
import pandas as pd
from joblib import load, dump
from scipy.sparse import hstack, csr_matrix
from tqdm import tqdm

import analyze
from tor_classifiers import TorClassifiers


def nopunct(text):
    return text.replace('/', '').replace(':', '').replace('.', '').replace('-', '').replace('=', '').replace('_', '')


if __name__ == "__main__":
    data_dir = Path(__file__).parent.resolve() / 'dockerdata'  # as_posix()
    temp_dir = Path(__file__).parent.resolve() / '.temp'

    # set up for classifiers
    all_name = '2021-10-13_all_ground_truth_labeled.csv'
    tc = TorClassifiers(all_name)

    # set up for collected data import
    # dfname_base = '2021-10-31_data.pkl'
    # data_dfs = []
    # existing_files = set()
    #
    # # import tgz files from boinc and compute basic features
    # for dfname in glob.glob("*-" + dfname_base):
    #     existing_df = pickle.load(open(dfname, 'rb'))
    #     existing_files.update(existing_df['tgzname'])
    #
    # tgz_files = sorted(os.listdir(data_dir))
    #
    # for i, tgz in enumerate(tqdm(tgz_files, desc='import')):
    #     if tgz in existing_files:
    #         # skip, we have already imported this one
    #         continue
    #     # print(f"importing {tgz}")
    #     temp_dir.mkdir(exist_ok=True)
    #     tar = tarfile.open(name=os.path.join(data_dir, tgz))
    #     try:
    #         tar.extractall(temp_dir)
    #     except OSError:
    #         print("error, skipping ", tgz)
    #         continue
    #     data = analyze.read_data(temp_dir.as_posix())
    #     data = analyze.compute_features(data)
    #     data['tgzname'] = tgz
    #     data_dfs.append(data)
    #
    #     shutil.rmtree(temp_dir)
    #
    #     # create a new dump every 1000 tgz files, to avoid memory error
    #     if i % 1000 == 0:
    #         df = pd.concat(data_dfs)
    #         df.reset_index(inplace=True, drop=True)
    #         print(f"File {i}, {len(df)} lines")
    #         pickle.dump(df, open('1-' + str(i) + '-' + dfname, 'wb'))
    #         data_dfs = []
    #
    # df = pd.concat(data_dfs)
    # df.reset_index(inplace=True, drop=True)
    # pickle.dump(df, open(dfname, 'wb'))

    # compute diff features and label with 6 classifiers
    for basename in sorted(glob.glob("*-2021-10-31_data.pkl")):
        print(basename)

        if os.path.exists(basename[:-4] + '_with_diff.joblib'):
            df = load(basename[:-4] + '_with_diff.joblib')
        else:
            df = load(basename)

            df = analyze.compute_diff_features(df, only_first=True)

            # find which features are used by the classifiers, part 1: find named columns from feature_elim dictionary
            featuresets = ['f1rc1', 'f1tc4', 'f2tc1', 'f2tc4', 'f3rc1', 'f3rc4', 'f5rc1', 'f6tc1']
            feature_cols = set()
            for f in featuresets:
                feature_cols.update(tc.feature_elim[f]['cols'])
            feature_cols.update(tc.f4_df_3.columns[9:])
            feature_cols.update(tc.f5_df_3.columns[9:])
            feature_cols.update(tc.f6_df_3.columns[9:])

            cols = [c for c in df.columns if c.startswith('tag_')]
            cols.extend([c for c in df.columns if c.startswith('status_')])
            for col in cols:
                df.loc[pd.isna(df[col]), col] = 0
            cols.extend(['num_requests', 'num_cookie_reqs', 'num_cookie_resps', 'num_tags'])

            used_cols = [c for c in cols if c + '_diff' in feature_cols]

            # compute only those diffs that are actually used in classifiers
            rows = []
            for group in tqdm(df.groupby(by=['topsite', 'subsite']), desc='diff features (2)'):
                # group[0] is a tuple with the topsite and subsite
                site = " ".join(group[0])
                # group[1] is a data frame with the data for the group
                firefox = group[1][group[1].browser == 'Firefox']
                tor = group[1][group[1].browser == 'Tor Browser']
                for i, row in tor.iterrows():
                    rowi = [i]
                    for col in used_cols:
                        rowi.append(firefox[col].values[0] - row[col])
                        # df.loc[i, col + '_diff'] = firefox[col].values[0] - row[col]

                    if not pd.isna(row.script_sources) and not pd.isna(firefox['script_sources'].values[0]):
                        tor_scripts = set(eval(row['script_sources']))
                        ff_scripts = set(eval(firefox['script_sources'].values[0]))
                        diff = tor_scripts.difference(ff_scripts)
                        rowi.append(str(diff))
                        # df.loc[i, 'script_sources_diff'] = str(diff)

                        tor_scripts = set(eval(row['script_domains']))
                        ff_scripts = set(eval(firefox['script_domains'].values[0]))
                        diff = tor_scripts.difference(ff_scripts)
                        rowi.append(str(diff))
                        # df.loc[i, 'script_domains_diff'] = str(diff)

                        tor_scripts = set(eval(row['script_paths']))
                        ff_scripts = set(eval(firefox['script_paths'].values[0]))
                        diff = tor_scripts.difference(ff_scripts)
                        rowi.append(str(diff))
                        # df.loc[i, 'script_paths_diff'] = str(diff)
                    else:
                        rowi.extend([str(set()), str(set()), str(set())])

                    # textual similarity (estimate of jaccard index computed using MinHash), following Niaki 2019 p7
                    if not pd.isna(row.text) and not pd.isna(firefox['text'].values[0]):
                        tor_textlist = row['text'].split(' ')
                        tor_minhash = datasketch.MinHash()
                        for d in tor_textlist:
                            tor_minhash.update(d.encode('utf8'))
                        ff_textlist = firefox['text'].values[0].split(' ')
                        ff_minhash = datasketch.MinHash()
                        for d in ff_textlist:
                            ff_minhash.update(d.encode('utf8'))
                        rowi.append(tor_minhash.jaccard(ff_minhash))
                        # df.loc[i, 'text_minhash'] = tor_minhash.jaccard(ff_minhash)

                        # difference in text (words that appear on Tor page but not on FF page)
                        text_diff = set(tor_textlist).difference(ff_textlist)
                        rowi.append(" ".join(list(text_diff)))
                        # df.loc[i, 'text_diff'] = " ".join(list(text_diff))
                    else:
                        rowi.extend([0, ''])

                    rows.append(rowi)

            colnames = ['idx']
            colnames.extend([col + '_diff' for col in used_cols])
            colnames.extend(['script_sources_diff', 'script_domains_diff', 'script_paths_diff', 'text_minhash', 'text_diff'])
            newdf = pd.DataFrame(rows)
            newdf.columns = colnames
            newdf.set_index('idx', inplace=True)
            df = df.join(newdf)
            dump(df, basename[:-4] + '_with_diff.joblib')

        configs = [('f1', 'f1rc1', 'c1', True, False, False),
                   ('f1', 'f1tc1', 'tc1', True, False, False),
                   ('f1', 'f1tc4', 'c4', True, False, False),
                   ('f2', 'f2tc1', 'c1', False, False, False),
                   ('f2', 'f2tc4', 'c4', False, False, False),
                   ('f3', 'f3rc1', 'c1', True, True, False),
                   ('f3', 'f3rc4', 'c4', True, True, False),
                   ('f4', 'f4', 'c1', True, False, True),
                   ('f4', 'f4', 'c4', True, False, True),
                   ('f5', 'f5rc1', 'c1', False, True, False),
                   ('f5', 'f5', 'c4', False, True, False),
                   ('f6', 'f6tc1', 'c1', False, False, True),
                   ('f6', 'f6', 'c4', False, False, True)]
        configs = pd.DataFrame(configs, columns=['feature_class', 'featureset', 'classifier', 'diff', 'html_only', 'http_only'])

        if os.path.exists(basename[:-4] + '_with_labels.joblib'):
            df = load(basename[:-4] + '_with_labels.joblib')
        else:

            labels = []
            for i, conf in configs.iterrows():
                print("Labeling", conf.featureset)
                cols = tc.get_feature_columns(df, diff=conf['diff'], html_only=conf.html_only, http_only=conf.http_only)
                clf = load(f'models/{conf.feature_class}{conf.classifier}_clf.joblib')
                x = df[(df.browser == 'Tor Browser') & (~pd.isna(df.text)) & (~pd.isna(df.first_status)) & (
                    ~pd.isna(df.perc2xx_diff)) & (~pd.isna(df.unif_diff))]

                if conf.feature_class in ['f1', 'f2', 'f3', 'f5']:
                    vectorizer_basename = 'vectorizers/2021-11-19_vectorizer-'
                    vecname = ''
                    if conf['diff']:
                        vecname += 'diff-'
                    if conf.html_only:
                        vecname += 'htmlonly-'
                    if conf.http_only:
                        vecname += 'httponly-'

                    vectorizer = pickle.load(open(vectorizer_basename + vecname + 'text.pkl', 'rb'))
                    x_text = vectorizer.transform(list(x.text))
                    vectorizer = pickle.load(open(vectorizer_basename + vecname + 'text-diff.pkl', 'rb'))
                    x_text_diff = vectorizer.transform(list(x.text_diff))

                    vectorizer2 = pickle.load(open(vectorizer_basename + vecname + 'source-diff.pkl', 'rb'))
                    x_source_diff = vectorizer2.transform([nopunct(" ".join(eval(sd))) for sd in x.script_sources_diff])
                    vectorizer2 = pickle.load(open(vectorizer_basename + vecname + 'path-diff.pkl', 'rb'))
                    x_path_diff = vectorizer2.transform([nopunct(" ".join(eval(sd))) for sd in x.script_paths_diff])
                    vectorizer2 = pickle.load(open(vectorizer_basename + vecname + 'domain-diff.pkl', 'rb'))
                    x_domain_diff = vectorizer2.transform([nopunct(" ".join(eval(sd))) for sd in x.script_domains_diff])

                    vectorizer2 = pickle.load(open(vectorizer_basename + vecname + 'source.pkl', 'rb'))
                    x_scriptsource = vectorizer2.transform([" ".join(eval(ss)) for ss in x.script_sources])
                    vectorizer2 = pickle.load(open(vectorizer_basename + vecname + 'domain.pkl', 'rb'))
                    x_scriptdomain = vectorizer2.transform([" ".join(eval(ss)) for ss in x.script_domains])
                    vectorizer2 = pickle.load(open(vectorizer_basename + vecname + 'path.pkl', 'rb'))
                    x_scriptpath = vectorizer2.transform([" ".join(eval(ss)) for ss in x.script_paths])
                    if conf['diff']:
                        script_names = ['source_diff', 'path_diff', 'domain_diff', 'scriptsource', 'scriptdomain', 'scriptpath']
                    else:
                        script_names = ['scriptsource', 'scriptdomain', 'scriptpath']
                    cols3 = tc.get_feature_columns(eval(f'tc.{conf.feature_class}_df_3'),
                                                   diff=conf['diff'], html_only=conf.html_only, http_only=conf.http_only)
                    cols_noscript = [c for c in cols3 if not c.startswith(tuple(script_names))]
                    for c in cols_noscript:
                        if c not in x.columns:
                            x[c] = 0

                    x_noscript = csr_matrix(x[cols_noscript])  # This reorders the columns to ensure the mask is correct
                    if conf['diff']:
                        all_features = hstack(
                            [x_text_diff, x_text, x_noscript, x_source_diff, x_path_diff, x_domain_diff, x_scriptsource,
                             x_scriptdomain, x_scriptpath])
                    else:
                        all_features = hstack([x_text, x_noscript, x_scriptsource, x_scriptdomain, x_scriptpath])

                    if 't' in conf.featureset or 'r' in conf.featureset:
                        mask = tc.feature_elim[conf.featureset]['mask']
                        matrix = all_features.tocsr()[:, mask]
                    else:
                        matrix = all_features.tocsr()
                else:
                    # F4 and F6 don't use text features
                    if 't' in conf.featureset or 'r' in conf.featureset:
                        cols2 = tc.feature_elim[conf.featureset]['cols']
                    else:
                        cols2 = tc.get_feature_columns(eval(f'tc.{conf.feature_class}_df_3'),
                                                       diff=conf['diff'], html_only=conf.html_only, http_only=conf.http_only)

                    new_cols = [c for c in cols2 if c not in cols]
                    for c in new_cols:
                        x[c] = 0

                    matrix = csr_matrix(x[cols2])  # This reorders the columns to ensure the mask is correct

                pred_labels = clf.predict(matrix)
                x[f"{conf.feature_class}_{conf.classifier}"] = pred_labels
                labels.append(x[[f"{conf.feature_class}_{conf.classifier}"]])

            for label_df in labels:
                df = df.join(label_df)

            dump(df, basename[:-4] + '_with_labels.joblib')

    # # Finding columns that have nan values
    # for col in cols:
    #     num_nan = sum(pd.isna(x[col]))
    #     if num_nan > 0:
    #         print(col, num_nan)
